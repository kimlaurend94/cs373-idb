.DEFAULT_GOAL := build
.PHONY: build, run

TAG=cs373-idb/mealmaker

build:
	docker compose build

run:
	docker compose up

push-noci:
	git push -o ci.skip

load-db-win:
	psql -h localhost -p 5432 -U user -d production -f ./db/init.dump