# cs373-idb-11-3
**Note**: the frontend is located in `/app`, and the backend is located in `/api`. Further, our Mocha and Selenium tests are located in `/app/test`.

## Canvas/Discord Group
IDB Group 11am 3

## Team Members
- Brice Chen bc34663, @Tomruler
- Finn Frankis fdf255, @FinnitoProductions
- Alex Burton amb9496, @AMACB
- Lauren Kim ldk532, @kimlaurend94

Project Leader Phase 1: Finn Frankis
Project Leader Phase 2: Alex Burton
Project Leader Phase 3: Lauren Kim
Project Leader Phase 4: Brice Chen

## Project Name
Mealmaker

## Gitlab URL
https://gitlab.com/kimlaurend94/cs373-idb

## Git ~~SHA~~ Tags
- [tags/phase-1](https://gitlab.com/kimlaurend94/cs373-idb/-/tags/phase-1)
- [tags/phase-2](https://gitlab.com/kimlaurend94/cs373-idb/-/tags/phase-2)
- [tags/phase-3](https://gitlab.com/kimlaurend94/cs373-idb/-/tags/phase-3)
- [tags/phase-4](https://gitlab.com/kimlaurend94/cs373-idb/-/tags/phase-4)

## Pipeline
https://gitlab.com/kimlaurend94/cs373-idb/-/pipelines

## Website
https://mealmaker.me/

## Postman Documentation
https://documenter.getpostman.com/view/23542695/2s83tFHXMw

## Phase 1 Time
- Brice: 5 estimated, 3 actual
- Finn: 4 estimated, 9 actual
- Alex: 8 estimated, 10 actual
- Lauren: 5 estimated, 3 actual

## Phase 2 Time
- Brice: 15 estimated, 11 actual
- Finn: 9 estimated, 12 actual
- Alex: 15 estimated, 15 actual
- Lauren: 15 estimated, 15 actual

## Phase 3 Time
- Brice: 12 estimated, 10 actual
- Finn: 4 estimated, 11 actual
- Alex: 15 estimated, 8 actual
- Lauren: 15 estimated, 18 actual

## Phase 4 Time
- Brice: estimated 5, actual 2
- Finn: estimated 5, actual 3
- Alex: estimated 5, actual 6
- Lauren: estimated 5, actual 4

## Comments

## Proposal
A full package meal solution system, providing users with local grocery stores, ingredient analysis and filtering, and a huge set of recipes to choose from.

## APIs
- https://developer.edamam.com/food-database-api — food & nutrition data
- https://rapidapi.com/apidojo/api/tasty/ — recipes with food + grocery stores
- https://rapidapi.com/zestfuldata/api/recipe-and-ingredient-analysis/ — recipe ingredient parser api

## Models
- Stores (>50k)
- Ingredients (>10k)
- Recipes (Several Million)

## Filter/Sortable Attributes
- Stores 
    - Price (low to high)
    - Distance (to me)
    - Number of items on sale
    - Review Stars
    - Time till closing

- Ingredients
    - Cost
    - Unit size
    - Calories
    - Carbohydrates
    - Sugar
    - Protein
    - Fat

- Recipes
    - Prep time
    - Cook time
    - Total time
    - Servings
    - Ingredients
    - Equipment
    - Macros (e.g. percent carbs, protein, fat)

## Searchable Attributes
- Stores 
    - Name
    - Store Chain
    - Type of Store
    - Online Options
    - Curbside Pickup or Delivery

- Ingredients
    - Name
    - Description
    - Dietary Constraints
    - Particularly high vitamin/mineral content
    - Food category 

- Recipes
    - Prep time
    - Cook time
    - Total time
    - Servings
    - Ingredients
    - Equipment
    - Macros (e.g. percent carbs, protein, fat)

## Media
- Store
    - Image
    - Google Maps
- Ingredient
    - Image
    - Shopping link
- Prepared Food
    - Image
    - Video

## Synthesis Questions
- What meals can I make with what I have on hand?
- What do I need to purchase from the store to make the meal I want?
- Which store is the best choice for the meal I want to make?
