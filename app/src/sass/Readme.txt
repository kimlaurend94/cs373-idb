SASS is a language that compiles into css code.
You need the VSC extension "Live SASS Compiler" to compile the files.

Files names must be terminated with .module.scss or .module.sass in order to compile into .module.css,
which is the only css file format React recognizes.

Inside the react files, use className={file.class} as arguments for tags; ie <div className={default_styles.body}>...

To make the files go to the correct directory, go to settings in VSC, find Live SASS Compiler, and replace
"liveSassCompile.settings.formats": [
        


    
        {
            "format": "expanded",
            "extensionName": none
            "savePath": none
            "savePathReplacementPairs": null
        }
    ]

with

"liveSassCompile.settings.formats": [
        


    
        {
            "format": "expanded",
            "extensionName": ".css",
            "savePath": "/css",
            "savePathReplacementPairs": null
        }
    ]

