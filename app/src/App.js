import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Table from 'react-bootstrap/Table';
import About from './routes/about.js';
import Visualizations from './routes/visualizations.js';
import ProviderVisualizations from './routes/provider-visualizations.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import React, { useEffect, useState } from "react";
import {generateHtmlDataPointFromValue, generateHtmlBooleanFromValue, getStoreNameFromId, getIngredientNameFromId, getRecipeNameFromId} from './utils.js'
import general_css from './css/site_sass.module.css'
import Form from 'react-bootstrap/Form';
import Highlighter from "react-highlight-words";
import Card from 'react-bootstrap/Card';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function genGroceryCard(url, title, vals){
  return(<Card style={{ width: '18rem' }}>
    <Card.Img variant="top" src={url} />
    <Card.Body>
      <Card.Title>{title}</Card.Title>
      <Card.Text>
      <ul class="list-group">
      <li class="list-group-item"> <b>Rating</b> {vals[0]} </li>
      <li class="list-group-item"> <b>Phone</b> {vals[1]} </li>
      <li class="list-group-item"> <b>City</b> {vals[2]} </li>
      <li class="list-group-item"> <b>Delivery?</b> {vals[3]} </li>
      </ul>
      </Card.Text>
    </Card.Body>
  </Card>)
}

function genIngredientCard(url, title, vals){
  return(<Card style={{ width: '18rem' }}>
    <Card.Img variant="top" src={url} />
    <Card.Body>
      <Card.Title>{title}</Card.Title>
      <Card.Text>
      <ul class="list-group">
      <li class="list-group-item"> <b>Calories</b> {vals[0]} </li>
      <li class="list-group-item"> <b>Protein</b> {vals[1]} </li>
      <li class="list-group-item"> <b>Carbohydrates</b> {vals[2]} </li>
      <li class="list-group-item"> <b>Fiber</b> {vals[3]} </li>
      </ul>
      </Card.Text>
    </Card.Body>
  </Card>)
}

function genRecipeCard(url, title, vals){
  return(<Card style={{ width: '18rem' }}>
    <Card.Img variant="top" src={url} />
    <Card.Body>
      <Card.Title>{title}</Card.Title>
      <Card.Text>
      <ul class="list-group">
      <li class="list-group-item"> <b>Prep Time (mins)</b> {vals[0]} </li>
      <li class="list-group-item"> <b>Cook Time (mins)</b> {vals[1]} </li>
      <li class="list-group-item"> <b>Serving Size (people)</b> {vals[2]} </li>
      <li class="list-group-item"> <b>Occasion (time, food type, holiday, etc.)</b> {vals[3]} </li>
      </ul>
      </Card.Text>
    </Card.Body>
  </Card>)
}

export function generateHtmlListFromArray(arr, numbered) {
  if (arr.length === 0) {
    return "N/A"
  }

  let cls = "list-group"
  if (numbered) {
    cls = "list-group list-group-numbered"
  }

  return <ul class={cls}>
    {
      (() => {
        let retList = []
        for (let i = 0; i < arr.length; i++) {
          retList.push(<li class="list-group-item"> {arr[i]} </li>)
        }
        return retList
      })()
    }
  </ul>
}

export function generateLabelsFromArray(arr) {
  if (arr.length === 0) {
    return "N/A"
  }
  return <>{
    (() => {
     let retList = []
     for (let i = 0; i < arr.length; i++) {
       retList.push(<span class="badge bg-secondary mx-1"> {arr[i].replaceAll("_", " ")} </span>)
     }
     return retList
    })()
  }</>
}

function generateRecipePageData(recipe, storesList, ingredientsList) {
    return <Route exact path={`/recipes/recipe${recipe.recipe_id}`}>
    <div className={general_css.background}>
    <h1> {recipe.name} </h1>
    <img src={`${recipe.thumbnail_url}`} width={450}/>
    <h2> Description </h2>
    {recipe.description}
    <h2> Prep Time </h2>
    <p> {generateHtmlDataPointFromValue(recipe.prep_time, "mins")} </p>
    <h2>Cook Time</h2>
    <p> {generateHtmlDataPointFromValue(recipe.cook_time, "mins")} </p>
    <h2> Servings </h2>
    <p> {generateHtmlDataPointFromValue(recipe.num_servings, "people")} </p>
    <h2> Ingredients </h2>
    {generateHtmlListFromArray(recipe.ingredients)}
    <h2> Tutorial </h2>
    <video width="320" height="240" controls>
    <source src={recipe.video_url} type="video/mp4"/>
      Your browser does not support the video tag.
    </video>
    <h2> Instructions </h2>
    {generateHtmlListFromArray(recipe.instructions, true)}
    <h2> Health Labels </h2>
    {generateLabelsFromArray(recipe.health_labels)}
    <h2>Occasion (time, food type, holiday, etc.)</h2>
    {generateLabelsFromArray(recipe.occasion)}
    <h2>Cuisine</h2>
    {generateHtmlListFromArray(recipe.cuisine)}
    <h2> Related Grocery Stores </h2>
    {generateHtmlListFromArray(recipe.grocery_store_ids.map(id => <a href={`../stores/store${id}`}> {getStoreNameFromId(storesList, id)} </a>))}
    <h2> Related Ingredients </h2>
    {generateHtmlListFromArray(recipe.ingredient_ids.map(id => <a href={`../ingredients/ingredient${id}`}> {getIngredientNameFromId(ingredientsList, id)} </a>))}
  </div>
  </Route>
}

function generateRecipeTableEntries(recipesList, startEntry, endEntry, searchRecipesText) {
  let retList = []
  let searchWords = searchRecipesText.split(' ')

  for (let i = Math.max(0, startEntry); i < Math.min(recipesList.length, endEntry); i++) {
    let recipe = recipesList[i]
    retList.push(<tr>
      <th scope="row"><a href={`recipes/recipe${recipe.recipe_id}`}><Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={recipe.name}
  /></a></th>
          <td>{recipe.prep_time != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${recipe.prep_time}`}
  /> : "N/A"}</td>
          <td>{recipe.cook_time != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${recipe.cook_time}`}
  /> : "N/A"} </td>
          <td>{recipe.total_time != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${recipe.total_time}`}
  /> : "N/A"}</td>
          <td>{recipe.num_servings != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${recipe.num_servings}`}
  /> : "N/A"}</td>
    </tr>)
  }

  return retList
}

function generateIngredientsTableEntries(ingredientsList, startEntry, endEntry, searchIngredientsText) {
  let retList = []
  let searchWords = searchIngredientsText.split(' ')

  for (let i = Math.max(0, startEntry); i < Math.min(ingredientsList.length, endEntry); i++) {
    let ingredient = ingredientsList[i]
    retList.push(<tr>
      <th className="text-capitalize" scope="row"><a href={`ingredients/ingredient${ingredient.ingredient_id}`}> <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={ingredient.name}
  /> </a></th>
          <td>{ingredient.calories != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${ingredient.calories.toFixed(2)}`}
  /> : "N/A"}</td>
          <td>{ingredient.protein != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${ingredient.protein.toFixed(2)}`}
  /> : "N/A"}</td>
          <td>{ingredient.carbs != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${ingredient.carbs.toFixed(2)}`}
  /> : "N/A"}</td>
          <td>{ingredient.fat != null ? <Highlighter
      searchWords={searchWords}
      autoEscape={true}
      textToHighlight={`${ingredient.fat.toFixed(2)}`}
    /> : "N/A"}</td>
          <td>{ingredient.fiber != null ? <Highlighter
        searchWords={searchWords}
        autoEscape={true}
        textToHighlight={`${ingredient.fiber.toFixed(2)}`}
      /> : "N/A"}</td>
    </tr>)
  }

  return retList
}

function generateIngredientNutritionTable(ingredient){
//TODO: Move these to constants
  let dv_calories = 2000.0;
  let dv_protein = 50.0;
  let dv_fat = 78.0;
  let dv_carbs = 275.0;
  let dv_fiber = 28.0;

  return(
    <Table striped bordered hover size="sm">
      <thead>
        <tr>
          <th >Category</th>
          <th >Amount</th>
          <th >%dv</th>
        </tr>
        <tr>
          <th>Calories</th>
          <td>{ingredient.calories.toFixed(2)}kcal</td>
          <td>{(ingredient.calories/dv_calories).toFixed(2)}%</td>
        </tr>
        <tr>
          <th>Protein</th>
          <td>{ingredient.protein.toFixed(2)}g</td>
          <td>{(ingredient.protein/dv_protein).toFixed(2)}%</td>
        </tr>
        <tr>
          <th>Carbohydrates</th>
          <td>{ingredient.carbs.toFixed(2)}g</td>
          <td>{(ingredient.carbs/dv_carbs).toFixed(2)}%</td>
        </tr>
        <tr>
          <th>Fat</th>
          <td>{ingredient.fat.toFixed(2)}g</td>
          <td>{(ingredient.fat/dv_fat).toFixed(2)}%</td>
        </tr>
        <tr>
          <th>Fiber</th>
          <td>{ingredient.fiber.toFixed(2)}g</td>
          <td>{(ingredient.fiber/dv_fiber*100).toFixed(2)}%</td>
        </tr>
      </thead>
    </Table>
  )
}

function generateIngredientsPageData(ingredient, storesList, recipesList) {
  const cumulativeSum = (sum => value => sum += value)(0)

  let total_weight = 1.0 * (ingredient.fat + ingredient.protein + ingredient.carbs + ingredient.fiber)
  let ordered_weights = [ingredient.fat, ingredient.protein, ingredient.carbs, ingredient.fiber]
  let percentages = ordered_weights.map(val => val / total_weight)
  let degrees = percentages.map(val => val * 360)
  let degreesCumSum = degrees.map(cumulativeSum)

  return <Route exact path={`/ingredients/ingredient${ingredient.ingredient_id}`}>
    <div className={general_css.background}>
      <h1 className="text-capitalize"> {ingredient.name} </h1>
      <img src={ingredient.image_urls[0]} width={450}/>
      <h2> Diet Labels </h2>
      {generateLabelsFromArray(ingredient.diet_labels)}
      <h2> Health Labels </h2>
      {generateLabelsFromArray(ingredient.health_labels)}
      <h2>Cautions</h2>
      {generateLabelsFromArray(ingredient.cautions)}
      <h2>Nutritional Information</h2>
      {generateIngredientNutritionTable(ingredient)}
      <h2>Macro Distribution</h2>
      <div style={{width: "400px", height: "400px", backgroundImage: `conic-gradient(red 0deg, red ${degreesCumSum[0]}deg, orange ${degreesCumSum[0]}deg, orange ${degreesCumSum[1]}deg, green ${degreesCumSum[1]}deg, green ${degreesCumSum[2]}deg, blue ${degreesCumSum[2]}deg)`, borderRadius: "50%"}}></div>
      <h3>Legend</h3>
      <p style={{color:"red"}}>Fat</p>
      <p style={{color:"orange"}}>Protein</p>
      <p style={{color:"green"}}>Carbohydrates</p>
      <p style={{color:"blue"}}>Fiber</p>
      <h2> Related Recipes </h2>
      {generateHtmlListFromArray(ingredient.recipe_ids.map(id => <a href={`../recipes/recipe${id}`}> {getRecipeNameFromId(recipesList, id)} </a>))}
      <h2> Related Grocery Stores </h2>
      {generateHtmlListFromArray(ingredient.grocery_store_ids.map(id => <a href={`../stores/store${id}`}> {getStoreNameFromId(storesList, id)} </a>))}
  </div>
</Route>
}

function generateStoresTableEntries(storesList, startEntry, endEntry, searchStoresText) {
  let retList = []
  let searchWords = searchStoresText.split(' ')

  for (let i = Math.max(0, startEntry); i < Math.min(storesList.length, endEntry); i++) {
    let store = storesList[i]
    retList.push(<tr>
      <th scope="row"><a href={`stores/store${store.grocery_store_id}`}><Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${store.name}`}
  /></a></th>
      <td>{store.price != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${store.price}`}
  /> : "N/A"}</td>
        <td>{store.rating != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${store.rating}`}
  /> : "N/A"}</td>
        <td>{store.delivery != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${store.delivery}`}
  /> : "N/A"}</td> 
        <td> {store.curbside_pickup != null ? <Highlighter
    searchWords={searchWords}
    autoEscape={true}
    textToHighlight={`${store.curbside_pickup}`}
  /> : "N/A"}</td>
    </tr>)
  }

  return retList
}

function generateStorePageData(store, recipesList, ingredientsList) {
  return <Route exact path={`/stores/store${store.grocery_store_id}`}>
  <div className={general_css.background}>
            <h1> {store.name} </h1>
            <img src={`${store.image_urls[0]}`} width={450}/>
            <h2> Rating </h2>
            <p> {store.rating} </p>
            <h2> Phone Number </h2>
            <p> {generateHtmlDataPointFromValue(store.phone, "")} </p>
            <h2>City</h2>
            <p>{generateHtmlDataPointFromValue(store.city, "")}</p> 
            <h2>Address</h2>
            <p>{generateHtmlDataPointFromValue(store.address, "")}</p>
            <div style={{position:"relative",textAlign:"right",height:"500px",width:"600px"}}><div style={{overflow:"hidden",background:"none!important",height:"500px",width:"600px"}}><iframe width="600" height="500" style={{overflow:"hidden",background:"none!important",height:"500px",width:"600px"}} src={`https://maps.google.com/maps?q=${store.address}&t=&z=13&ie=UTF8&iwloc=&output=embed`} frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.whatismyip-address.com/divi-discount/"></a><br/><a href="https://www.embedgooglemap.net"></a></div></div>
            <h2> Delivery </h2>
            <p>{generateHtmlBooleanFromValue(store.delivery)}</p> 
            <h2> Curbside Pickup </h2>
            <p>{generateHtmlBooleanFromValue(store.curbside_pickup)}</p> 
            <h2>Hours</h2>
            <ul class="list-group">
            {generateHtmlListFromArray(store.opening_hours)}
            </ul>
            <h2>Website</h2>
            <a href={`${store.website}`}> {store.name} </a>
            <h2> Related Recipes </h2>
            {generateHtmlListFromArray(store.recipe_ids.map(id => <a href={`../recipes/recipe${id}`}> {getRecipeNameFromId(recipesList, id)} </a>))}
            <h2> Related Ingredients </h2>
            {generateHtmlListFromArray(store.ingredient_ids.map(id => <a href={`../ingredients/ingredient${id}`}> {getIngredientNameFromId(ingredientsList, id)} </a>))}
        </div>
</Route>
}

function updateSortIngredientsBy(newSortIngredientsBy, sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients) {
  if(newSortIngredientsBy === sortIngredientsBy) {
    setSortIngredientsDir((sortIngredientsDir + 1) % 3);
  } else {
    setSortIngredientsDir(1);
  }

  setSortIngredientsBy(newSortIngredientsBy);
  setShouldUpdateIngredients(true);
}

function updateSortRecipesBy(newSortRecipesBy, sortRecipesBy, setSortRecipesBy, sortRecipesDir, setSortRecipesDir, setShouldUpdateRecipes) {
  if(newSortRecipesBy === sortRecipesBy) {
    setSortRecipesDir((sortRecipesDir + 1) % 3);
  } else {
    setSortRecipesDir(1);
  }

  setSortRecipesBy(newSortRecipesBy);
  setShouldUpdateRecipes(true);
}


function updateSortStoresBy(newSortStoresBy, sortStoresBy, setSortStoresBy, sortStoresDir, setSortStoresDir, setShouldUpdateStores) {
  if(newSortStoresBy === sortStoresBy) {
    setSortStoresDir((sortStoresDir + 1) % 3);
  } else {
    setSortStoresDir(1);
  }

  setSortStoresBy(newSortStoresBy);
  setShouldUpdateStores(true);
}

function getSortSymbol(sortIngredientsBy, currSortIngredientsBy, sortDir) {
  return (sortDir == 0 || (sortIngredientsBy != currSortIngredientsBy)) ? "↑↓" : (sortDir == 1 ? "↑" : "↓")
}

function generateIngredientsTableHeader(updateSortIngredientsBy, sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients, shouldSort) {
  return <tr> <th scope="col" onClick={() => updateSortIngredientsBy("name", sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients)} style={{cursor:'pointer'}}>Ingredient Name {!shouldSort ? "" : getSortSymbol("name", sortIngredientsBy, sortIngredientsDir)}</th>
  <th scope="col" onClick={() => { updateSortIngredientsBy("calories", sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients) } } style={{cursor:'pointer'}}>Calories (kcal) {!shouldSort ? "" : getSortSymbol("calories", sortIngredientsBy, sortIngredientsDir)}</th>
  <th scope="col" onClick={() => { updateSortIngredientsBy("protein", sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients) } } style={{cursor:'pointer'}}>Protein (g) {!shouldSort ? "" : getSortSymbol("protein", sortIngredientsBy, sortIngredientsDir)} </th>
  <th scope="col" onClick={() => { updateSortIngredientsBy("carbs", sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients) } } style={{cursor:'pointer'}}>Carbohydrates (g) {!shouldSort ? "" : getSortSymbol("carbs", sortIngredientsBy, sortIngredientsDir)}</th>
  <th scope="col" onClick={() => { updateSortIngredientsBy("fat", sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients) } } style={{cursor:'pointer'}}>Fat (g) {!shouldSort ? "" : getSortSymbol("fat", sortIngredientsBy, sortIngredientsDir)}</th>
  <th scope="col" onClick={() => { updateSortIngredientsBy("fiber", sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients) } } style={{cursor:'pointer'}}>Fiber (g) {!shouldSort ? "" : getSortSymbol("fiber", sortIngredientsBy, sortIngredientsDir)}</th> </tr>
}

function generateRecipesTableHeader(updateSortRecipesBy, sortRecipesBy, setSortRecipesBy, sortRecipesDir, setSortRecipesDir, setShouldUpdateRecipes, shouldSort) {
  return <tr>
                  <th scope="col" onClick={() => updateSortRecipesBy("name", sortRecipesBy, setSortRecipesBy, sortRecipesDir, setSortRecipesDir, setShouldUpdateRecipes)} style={{cursor:'pointer'}}>Recipe Name {!shouldSort ? "" : getSortSymbol("name", sortRecipesBy, sortRecipesDir)}</th>
                  <th scope="col" onClick={() => { updateSortRecipesBy("prep_time", sortRecipesBy, setSortRecipesBy, sortRecipesDir, setSortRecipesDir, setShouldUpdateRecipes) } } style={{cursor:'pointer'}}> Prep Time (mins) {!shouldSort ? "" : getSortSymbol("prep_time", sortRecipesBy, sortRecipesDir)} </th>
                  <th scope="col" onClick={() => { updateSortRecipesBy("cook_time", sortRecipesBy, setSortRecipesBy, sortRecipesDir, setSortRecipesDir, setShouldUpdateRecipes) } } style={{cursor:'pointer'}}> Cook Time (mins) {!shouldSort ? "" : getSortSymbol("cook_time", sortRecipesBy, sortRecipesDir)} </th>
                  <th scope="col" onClick={() => { updateSortRecipesBy("total_time", sortRecipesBy, setSortRecipesBy, sortRecipesDir, setSortRecipesDir, setShouldUpdateRecipes) } } style={{cursor:'pointer'}}> Total Time (mins){!shouldSort ? "" : getSortSymbol("total_time", sortRecipesBy, sortRecipesDir)} </th>
                  <th scope="col" onClick={() => { updateSortRecipesBy("num_servings", sortRecipesBy, setSortRecipesBy, sortRecipesDir, setSortRecipesDir, setShouldUpdateRecipes) } } style={{cursor:'pointer'}}> Serving Size (people) {!shouldSort ? "" : getSortSymbol("num_servings", sortRecipesBy, sortRecipesDir)} </th>
                </tr>
}

function generateStoresTableHeader(updateSortStoresBy, sortStoresBy, setSortStoresBy, sortStoresDir, setSortStoresDir, setShouldUpdateStores, shouldSort) {
  return <tr>
  <th scope="col" onClick={() => updateSortStoresBy("name", sortStoresBy, setSortStoresBy, sortStoresDir, setSortStoresDir, setShouldUpdateStores)} style={{cursor:'pointer'}} disabled={!shouldSort}>Store Name {!shouldSort ? "" : getSortSymbol("name", sortStoresBy, sortStoresDir)}</th>
  <th scope="col" onClick={() => { updateSortStoresBy("price", sortStoresBy, setSortStoresBy, sortStoresDir, setSortStoresDir, setShouldUpdateStores) } } style={{cursor:'pointer'}} disabled={!shouldSort}>Price {!shouldSort ? "" : getSortSymbol("price", sortStoresBy, sortStoresDir)}</th>
  <th scope="col" onClick={() => { updateSortStoresBy("rating", sortStoresBy, setSortStoresBy, sortStoresDir, setSortStoresDir, setShouldUpdateStores) } } style={{cursor:'pointer'}} disabled={!shouldSort}>Rating {!shouldSort ? "" : getSortSymbol("rating", sortStoresBy, sortStoresDir)} </th>
  <th scope="col" onClick={() => { updateSortStoresBy("delivery", sortStoresBy, setSortStoresBy, sortStoresDir, setSortStoresDir, setShouldUpdateStores) } } style={{cursor:'pointer'}} disabled={!shouldSort}>Delivery? {!shouldSort ? "" : getSortSymbol("delivery", sortStoresBy, sortStoresDir)}</th>
  <th scope="col" onClick={() => { updateSortStoresBy("curbside_pickup", sortStoresBy, setSortStoresBy, sortStoresDir, setSortStoresDir, setShouldUpdateStores) } } style={{cursor:'pointer'}} disabled={!shouldSort}>Curbside Pickup? {!shouldSort ? "" : getSortSymbol("curbside_pickup", sortStoresBy, sortStoresDir)}</th>
</tr>
}

function updateIngredientsRadio(event, setFilterIngredientsBy, setFilterIngredientsText, setShouldUpdateIngredients) {
  setFilterIngredientsBy(event.target.id);
  setFilterIngredientsText("");
  setShouldUpdateIngredients(true);
}

function updateRecipesRadio(event, setFilterRecipesBy, setFilterRecipesText, setShouldUpdateRecipes) {
  setFilterRecipesBy(event.target.id);
  setFilterRecipesText("");
  setShouldUpdateRecipes(true);
}

function updateStoresRadio(event, setFilterStoresBy, setFilterStoresText, setShouldUpdateStores) {
  setFilterStoresBy(event.target.id);
  setFilterStoresText("");
  setShouldUpdateStores(true);
}

const RESULTS_PER_PAGE = 10
const RESULTS_ON_HOME_PAGE_PER_MODEL = 3

function BasicExample() {
  const [recipesData, setRecipesData] = useState(-1)
  const [storesData, setStoresData] = useState(-1)
  const [ingredientsData, setIngredientsData] = useState(-1)

  const [currIngredientsPage, setCurrIngredientsPage] = useState(0);
  const [sortIngredientsBy, setSortIngredientsBy] = useState('none');
  const [sortIngredientsDir, setSortIngredientsDir] = useState(0);
  const [shouldUpdateIngredients, setShouldUpdateIngredients] = useState(true);

  const [filterIngredientsBy, setFilterIngredientsBy] = useState('none')
  const [filterIngredientsText, setFilterIngredientsText] = useState('')

  const [searchIngredientsText, setSearchIngredientsText] = useState('')
  const [lockedInSearchIngredientsText, setLockedInSearchIngredientsText] = useState('')

  const [currRecipesPage, setCurrRecipesPage] = useState(0);
  const [sortRecipesBy, setSortRecipesBy] = useState('none');
  const [sortRecipesDir, setSortRecipesDir] = useState(0);
  const [shouldUpdateRecipes, setShouldUpdateRecipes] = useState(true);

  const [filterRecipesBy, setFilterRecipesBy] = useState('none')
  const [filterRecipesText, setFilterRecipesText] = useState('')

  const [searchRecipesText, setSearchRecipesText] = useState('')
  const [lockedInSearchRecipesText, setLockedInSearchRecipesText] = useState('')

  const [currStoresPage, setCurrStoresPage] = useState(0);
  const [sortStoresBy, setSortStoresBy] = useState('none');
  const [sortStoresDir, setSortStoresDir] = useState(0);
  const [shouldUpdateStores, setShouldUpdateStores] = useState(true);

  const [filterStoresBy, setFilterStoresBy] = useState('none')
  const [filterStoresText, setFilterStoresText] = useState('')

  const [searchStoresText, setSearchStoresText] = useState('')
  const [lockedInSearchStoresText, setLockedInSearchStoresText] = useState('')

  const [globalSearchText, setGlobalSearchText] = useState('')
  const [lockedInGlobalSearchText, setLockedInGlobalSearchText] = useState('')

  useEffect(() => {
    if (recipesData == -1 || shouldUpdateRecipes) {
        fetch(`${process.env.REACT_APP_API_URL}/recipes?${sortRecipesBy === 'none' || sortRecipesDir === 0 ? "": `&sort=${sortRecipesDir == 2 ? "desc-" : ""}${sortRecipesBy}`}${filterRecipesBy === 'none' || filterRecipesText === '' ? "" : `&${filterRecipesBy}=${filterRecipesText}`}${searchRecipesText === 'none' ? "" : `&search=${searchRecipesText}`}`).then(
          resp => resp.json()
        ).then(
          data => {setRecipesData(data); }
        ).catch((error) => {
        setRecipesData({'count': 0, 'page': []});
        }).finally(() => {
          setCurrRecipesPage(0); setShouldUpdateRecipes(false); setLockedInSearchRecipesText(searchRecipesText);
        })
  }})

  useEffect(() => {
    if (storesData == -1 || shouldUpdateStores) {
        fetch(`${process.env.REACT_APP_API_URL}/stores?${sortStoresBy === 'none' || sortStoresDir === 0 ? "": `&sort=${sortStoresDir == 2 ? "desc-" : ""}${sortStoresBy}`}${filterStoresBy === 'none' || filterStoresText === '' ? "" : `&${filterStoresBy}=${filterStoresText}`}${searchStoresText === 'none' ? "" : `&search=${searchStoresText}`}`).then(
          resp => resp.json()
        ).then(
          data => {console.log(data); setStoresData(data);}
        ).catch(error => {
        setStoresData({'count': 0, 'page': []}); 
      }).finally(() => {
        setCurrStoresPage(0); setShouldUpdateStores(false); setLockedInSearchStoresText(searchStoresText);
      })
  }})

  useEffect(() => {
    if (ingredientsData == -1 || shouldUpdateIngredients) {
        fetch(`${process.env.REACT_APP_API_URL}/ingredients?${sortIngredientsBy === 'none' || sortIngredientsDir === 0 ? "": `&sort=${sortIngredientsDir == 2 ? "desc-" : ""}${sortIngredientsBy}`}${filterIngredientsBy === 'none' || filterIngredientsText === '' ? "" : `&${filterIngredientsBy}=${filterIngredientsText}`}${searchIngredientsText === 'none' ? "" : `&search=${searchIngredientsText}`}`).then(
          resp => resp.json()
        ).then(
          data => {setIngredientsData(data);}
        ).catch(error => {
          setIngredientsData({'count': 0, 'page': []}); 
        }).finally(() => {
          setCurrIngredientsPage(0); setShouldUpdateIngredients(false); setLockedInSearchIngredientsText(searchIngredientsText);
        })
      }
    })

  useEffect(() => {
    document.title = "MealMaker"
  }, [])

  return (
    <Router >
    <Navbar collapseOnSelect className = {general_css.navbar} expand="lg" >
      <Container>
        <Navbar.Brand href="/">MealMaker</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/about" className={general_css.navbar__pane}>About</Nav.Link>
            <Nav.Link href="/stores" className={general_css.navbar__pane}>Groceries</Nav.Link>
            <Nav.Link href="/ingredients" className={general_css.navbar__pane}>Ingredients</Nav.Link>
            <Nav.Link href="/recipes" className={general_css.navbar__pane}>Recipes</Nav.Link>
            <Nav.Link href="/visualizations" className={general_css.navbar__pane}>Visualizations</Nav.Link>
            <Nav.Link href="/provider-visualizations" className={general_css.navbar__pane}>Provider Visualizations</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    <Switch>
        <Route exact path="/">
        <div className={general_css.background}>
    <div className={general_css.home_splash}>
      <div style = {{backgroundImage: "url(/Mealmaker Full Logo.png)"}}>
        {/* <img src={`Mealmaker Full Logo512x256.png`} alt="Chicken on mashed potatoes"/> */}
        <div className={general_css.titleCard__logo}>
          <img src={`Mealmaker Full Logo512x256.png`} className="mx-auto d-block" alt="Mealmaker Full Logo"/>
        </div>
      </div>
    </div>
    <div>
      <div className={general_css.home_info__desc}>
        <h1>Welcome to MealMaker!</h1>
        <p1>We encourage you to take a look at our grocery stores, 
          ingredients, and recipes to explore nutritious and delicious food options.</p1>
      </div>
      <hr/>
      <b> Search website: </b>
            <Form>
              <div key={`search`} className="mb-3">
               <Form.Control type="email" placeholder="Enter" value={globalSearchText} onInput={(e) => {setGlobalSearchText(e.target.value); }}/> 
               <Button variant="primary" onClick={() => {setShouldUpdateStores(true); setShouldUpdateRecipes(true); setShouldUpdateIngredients(true); setLockedInGlobalSearchText(globalSearchText); setSearchRecipesText(globalSearchText); setSearchStoresText(globalSearchText); setSearchIngredientsText(globalSearchText);} } disabled={globalSearchText == 'none'}> Search </Button>
              </div>
            </Form>
            {lockedInGlobalSearchText == 'none' || lockedInGlobalSearchText == '' ? "" : 
            <div>
            <h3> Matching Stores </h3>
            <table className="table">
<thead>
            {generateStoresTableHeader(updateSortStoresBy, sortStoresBy, setSortStoresBy, sortStoresDir, setSortStoresDir, setShouldUpdateStores, false)}
</thead>
              <tbody>
                {storesData != -1 ? generateStoresTableEntries(storesData.page, currStoresPage * RESULTS_ON_HOME_PAGE_PER_MODEL, (currStoresPage + 1) * RESULTS_ON_HOME_PAGE_PER_MODEL, lockedInSearchStoresText) : []}
              </tbody>
            </table>
        <h3> Matching Ingredients </h3>
        <table className="table">
            <thead>
            {generateIngredientsTableHeader(updateSortIngredientsBy, sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients, false)}
            </thead>
            <tbody>
              {ingredientsData != -1 ? generateIngredientsTableEntries(ingredientsData.page, currIngredientsPage * RESULTS_ON_HOME_PAGE_PER_MODEL, (currIngredientsPage + 1) * RESULTS_ON_HOME_PAGE_PER_MODEL, lockedInSearchIngredientsText) : []}
            </tbody>
        </table>
      <h3> Matching Recipes </h3>
      <table className="table">
        <thead>
          {generateRecipesTableHeader(updateSortRecipesBy, sortRecipesBy, setSortRecipesBy, sortRecipesDir, setSortRecipesDir, setShouldUpdateRecipes, false)}
        </thead>
        <tbody>
          {recipesData != -1 ? generateRecipeTableEntries(recipesData.page, currRecipesPage * RESULTS_ON_HOME_PAGE_PER_MODEL, (currRecipesPage + 1) * RESULTS_ON_HOME_PAGE_PER_MODEL, lockedInSearchRecipesText) : []}
        </tbody>
      </table>
      </div>}
      <hr/>
      <div className={general_css.models}>
        {genGroceryCard('https://phoeniciafoods.com/wp-content/gallery/westheimer-market/westheimerstore2.jpg', 'Phoenicia Specialty Foods', [4.6, "(832) 360-2222", "Houston", "Yes"])}
        {genIngredientCard('https://cdn.apartmenttherapy.info/image/fetch/f_auto,q_auto:eco,w_1500,c_fill,g_auto/https://storage.googleapis.com/gen-atmedia/3/2018/09/c9b3c5287417c2c40763ace03009ec7f50fb051b.jpeg', 'Chicken Breast', [160, 12, 8, 0.5])}
        {genRecipeCard('https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/403998.jpg', 'Salted Caramel Apple Bark', [25,15,4,"Desserts"])}
      </div>
    </div>
  </div>
        </Route>
        <Route exact path="/about">
          <About/>
        </Route>
        <Route exact path="/stores">
        {
          (() => {
            return     <div className={general_css.background}>
            <b> Search by: </b>
            <Form>
              <div key={`stores-search`} className="mb-3">
               <Form.Control type="email" placeholder="Enter" value={searchStoresText} onInput={(e) => setSearchStoresText(e.target.value)}/> 
               <Button variant="primary" onClick={() => setShouldUpdateStores(true)} disabled={searchStoresText == 'none'}> Search </Button>
              </div>
            </Form>
            <table className="table">
<thead>
            {generateStoresTableHeader(updateSortStoresBy, sortStoresBy, setSortStoresBy, sortStoresDir, setSortStoresDir, setShouldUpdateStores, true)}
</thead>
              <tbody>
                {storesData != -1 ? generateStoresTableEntries(storesData.page, currStoresPage * RESULTS_PER_PAGE, (currStoresPage + 1) * RESULTS_PER_PAGE, lockedInSearchStoresText) : []}
              </tbody>
            </table>
            <b> Filter by: </b>
            <Form>
            {['radio'].map((type) => (
              <div key={`inline-${type}`} className="mb-3">
                <Form.Check inline label="Store Name" name="group1" type={type} id={`name`} onChange={(e) => updateStoresRadio(e, setFilterStoresBy, setFilterStoresText, setShouldUpdateStores)} />
                <Form.Check inline label="Price" name="group1" type={type} id={`price`} onChange={(e) => updateStoresRadio(e, setFilterStoresBy, setFilterStoresText, setShouldUpdateStores)} />
                <Form.Check inline label="Rating" name="group1" type={type} id={`rating`} onChange={(e) => updateStoresRadio(e, setFilterStoresBy, setFilterStoresText, setShouldUpdateStores)} />
                <Form.Check inline label="Delivery" name="group1" type={type} id={`delivery`} onChange={(e) => updateStoresRadio(e, setFilterStoresBy, setFilterStoresText, setShouldUpdateStores)}/>
                <Form.Check inline label="Curbside Pickup" name="group1" type={type} id={`curbside_pickup`} onChange={(e) => updateStoresRadio(e, setFilterStoresBy, setFilterStoresText, setShouldUpdateStores)}/>
               <Form.Control type="email" placeholder="Enter" value={filterStoresText} onInput={(e) => setFilterStoresText(e.target.value)}/> 
               <Button variant="primary" onClick={() => setShouldUpdateStores(true)} disabled={filterStoresBy == 'none'}> Filter </Button>
              </div>
            ))}
            </Form>

            <b>Total results: </b> {Math.min((currStoresPage + 1) * RESULTS_PER_PAGE, storesData.count) - Math.max(currStoresPage * RESULTS_PER_PAGE, 0)} of {storesData == -1 ? 0 : storesData.count} <br/>
            <b>Page: </b> {currStoresPage + 1} of {Math.ceil((storesData.count * 1.0) / RESULTS_PER_PAGE)} <br/>
            <Button variant="primary" onClick={() => setCurrStoresPage(currStoresPage - 1)} disabled={currStoresPage <= 0}> Previous </Button>
            <Button variant="primary" onClick={() => setCurrStoresPage(currStoresPage + 1)} disabled={(currStoresPage + 1) * RESULTS_PER_PAGE >= storesData.count}> Next </Button>
            </div>
          })()
        }
        </Route>
        {
            (() => {          
              if (storesData == -1 || recipesData == -1 || ingredientsData == -1 || storesData.count === undefined) {
                return []
              }
              
              let retList = []
              for (let i = 0; i < storesData.count; i++) {
                retList.push(generateStorePageData(storesData.page[i], recipesData.page, ingredientsData.page))
              }
              return retList
          })()
        }
        <Route exact path="/ingredients">
        {
          (() => {
            return <div className={general_css.background}>
            <b> Search by: </b>
            <Form>
              <div key={`ingredients-search`} className="mb-3">
               <Form.Control type="email" placeholder="Enter" value={searchIngredientsText} onInput={(e) => setSearchIngredientsText(e.target.value)}/> 
               <Button variant="primary" onClick={() => setShouldUpdateIngredients(true)} disabled={searchIngredientsText == 'none'}> Search </Button>
              </div>
            </Form>
            <table className="table">
            <thead>
            {generateIngredientsTableHeader(updateSortIngredientsBy, sortIngredientsBy, setSortIngredientsBy, sortIngredientsDir, setSortIngredientsDir, setShouldUpdateIngredients, true)}
            </thead>
              <tbody>
                {ingredientsData != -1 ? generateIngredientsTableEntries(ingredientsData.page, currIngredientsPage * RESULTS_PER_PAGE, (currIngredientsPage + 1) * RESULTS_PER_PAGE, lockedInSearchIngredientsText) : []}
              </tbody>
            </table>
            <b> Filter by: </b>
            <Form>
            {['radio'].map((type) => (
              <div key={`inline-${type}`} className="mb-3">
                <Form.Check inline label="Ingredient Name" name="group1" type={type} id={`name`} onChange={(e) => updateIngredientsRadio(e, setFilterIngredientsBy, setFilterIngredientsText, setShouldUpdateIngredients)} />
                <Form.Check inline label="Calories" name="group1" type={type} id={`calories`} onChange={(e) => updateIngredientsRadio(e, setFilterIngredientsBy, setFilterIngredientsText, setShouldUpdateIngredients)} />
                <Form.Check inline label="Protein" name="group1" type={type} id={`protein`} onChange={(e) => updateIngredientsRadio(e, setFilterIngredientsBy, setFilterIngredientsText, setShouldUpdateIngredients)} />
                <Form.Check inline label="Carbohydrates" name="group1" type={type} id={`carbohydrates`} onChange={(e) => updateIngredientsRadio(e, setFilterIngredientsBy, setFilterIngredientsText, setShouldUpdateIngredients)}/>
                <Form.Check inline label="Fiber" name="group1" type={type} id={`fiber`} onChange={(e) => updateIngredientsRadio(e, setFilterIngredientsBy, setFilterIngredientsText, setShouldUpdateIngredients)}/>
               <Form.Control type="email" placeholder="Enter" value={filterIngredientsText} onInput={(e) => setFilterIngredientsText(e.target.value)}/> 
               <Button variant="primary" onClick={() => setShouldUpdateIngredients(true)} disabled={filterIngredientsBy == 'none'}> Filter </Button>
              </div>
            ))}
            </Form>

            <b>Total results: </b> {Math.min((currIngredientsPage + 1) * RESULTS_PER_PAGE, ingredientsData.count) - Math.max(currIngredientsPage * RESULTS_PER_PAGE, 0)} of {ingredientsData == -1 ? 0 : ingredientsData.count} <br/>
            <b>Page: </b> {currIngredientsPage + 1} of {Math.ceil((ingredientsData.count * 1.0) / RESULTS_PER_PAGE)} <br/>
            <Button variant="primary" onClick={() => setCurrIngredientsPage(currIngredientsPage - 1)} disabled={currIngredientsPage <= 0}> Previous </Button>
            <Button variant="primary" onClick={() => setCurrIngredientsPage(currIngredientsPage + 1)} disabled={(currIngredientsPage + 1) * RESULTS_PER_PAGE >= ingredientsData.count}> Next </Button>
            </div>
          })()
        }
        </Route>
        {
            (() => {          
              if (storesData == -1 || recipesData == -1 || ingredientsData == -1 || ingredientsData.count === undefined) {
                return []
              }  
              
              let retList = []
              for (let i = 0; i < ingredientsData.count; i++) {
                retList.push(generateIngredientsPageData(ingredientsData.page[i], storesData.page, recipesData.page))
              }
              return retList
          })()
        }
        <Route exact path="/recipes">
        {
          (() => {
            return <div className={general_css.background}>
            <b> Search by: </b>
            <Form>
              <div key={`recipes-search`} className="mb-3">
               <Form.Control type="email" placeholder="Enter" value={searchRecipesText} onInput={(e) => setSearchRecipesText(e.target.value)}/> 
               <Button variant="primary" onClick={() => setShouldUpdateRecipes(true)} disabled={searchRecipesText == 'none'}> Search </Button>
              </div>
            </Form>
            <table className="table">
              <thead>
                {generateRecipesTableHeader(updateSortRecipesBy, sortRecipesBy, setSortRecipesBy, sortRecipesDir, setSortRecipesDir, setShouldUpdateRecipes, true)}
              </thead>
              <tbody>
                {recipesData != -1 ? generateRecipeTableEntries(recipesData.page, currRecipesPage * RESULTS_PER_PAGE, (currRecipesPage + 1) * RESULTS_PER_PAGE, lockedInSearchRecipesText) : []}
              </tbody>
            </table>

            <b> Filter by: </b>
            <Form>
            {['radio'].map((type) => (
              <div key={`inline-${type}`} className="mb-3">
                <Form.Check inline label="Recipe Name" name="group1" type={type} id={`name`} onChange={(e) => updateRecipesRadio(e, setFilterRecipesBy, setFilterRecipesText, setShouldUpdateRecipes)} />
                <Form.Check inline label="Prep Time" name="group1" type={type} id={`prep_time`} onChange={(e) => updateRecipesRadio(e, setFilterRecipesBy, setFilterRecipesText, setShouldUpdateRecipes)} />
                <Form.Check inline label="Cook Time" name="group1" type={type} id={`cook_time`} onChange={(e) => updateRecipesRadio(e, setFilterRecipesBy, setFilterRecipesText, setShouldUpdateRecipes)} />
                <Form.Check inline label="Total Time" name="group1" type={type} id={`total_time`} onChange={(e) => updateRecipesRadio(e, setFilterRecipesBy, setFilterRecipesText, setShouldUpdateRecipes)}/>
                <Form.Check inline label="Serving Size" name="group1" type={type} id={`num_servings`} onChange={(e) => updateRecipesRadio(e, setFilterRecipesBy, setFilterRecipesText, setShouldUpdateRecipes)}/>
               <Form.Control type="email" placeholder="Enter" value={filterRecipesText} onInput={(e) => setFilterRecipesText(e.target.value)}/> 
               <Button variant="primary" onClick={() => setShouldUpdateRecipes(true)} disabled={filterRecipesBy == 'none'}> Filter </Button>
              </div>
            ))}
            </Form>

            <b>Total results: </b> {Math.min((currRecipesPage + 1) * RESULTS_PER_PAGE, recipesData.count) - Math.max(currRecipesPage * RESULTS_PER_PAGE, 0)} of {recipesData == -1 ? 0 : recipesData.count} <br/>
            <b>Page: </b> {currRecipesPage + 1} of {Math.ceil((recipesData.count * 1.0) / RESULTS_PER_PAGE)} <br/>
            <Button variant="primary" onClick={() => setCurrRecipesPage(currRecipesPage - 1)} disabled={currRecipesPage <= 0}> Previous </Button>
            <Button variant="primary" onClick={() => setCurrRecipesPage(currRecipesPage + 1)} disabled={(currRecipesPage + 1) * RESULTS_PER_PAGE >= recipesData.count}> Next </Button>
            </div>
          })()
        }
        </Route>
        {
            (() => {          
              if (storesData == -1 || recipesData == -1 || ingredientsData == -1 || recipesData.count === undefined) {
                return []
              }  
              
              let retList = []
              for (let i = 0; i < recipesData.count; i++) {
                retList.push(generateRecipePageData(recipesData.page[i], storesData.page, ingredientsData.page))
              }
              return retList
          })()
        }
        <Route exact path="/visualizations">
          <Visualizations/>
        </Route>
        <Route exact path="/provider-visualizations">
          <ProviderVisualizations/>
        </Route>
      </Switch>
    </Router>
  );
}


export default BasicExample;

