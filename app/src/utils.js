export function generateHtmlDataPointFromValue(nums, unit) {
    if (nums == null) {
      return "N/A"
    }
    if (unit == "") {
        return `${nums}`
    }
  
    return `${nums} ${unit}`
}
  
export function generateHtmlBooleanFromValue(boolVal) {
    if (boolVal == null) {
        return "N/A"
    }

    return boolVal ? "Yes" : "No"
}

export function getStoreNameFromId(storesList, id) {
    for (let i = 0; i < storesList.length; i++) {
      if (storesList[i].grocery_store_id == id) {
        return storesList[i].name
      }
    }
    return "No Name Provided"
  }
  
export function getIngredientNameFromId(ingredientsList, id) {
    for (let i = 0; i < ingredientsList.length; i++) {
      if (ingredientsList[i].ingredient_id == id) {
        return ingredientsList[i].name
      }
    }
    return "No Name Provided"
}
  
export function getRecipeNameFromId(recipesList, id) {
    for (let i = 0; i < recipesList.length; i++) {
      if (recipesList[i].recipe_id == id) {
        return recipesList[i].name
      }
    }
    return "No Name Provided"
}