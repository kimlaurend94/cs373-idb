import React, { useEffect, useState } from "react";
import { Button, Card, Container, Table } from "react-bootstrap";

import general_css from '../css/site_sass.module.css'
import GitLabImg from '../GitLabImg.png';
import GoogleMapsImg from '../GoogleMapsImg.png';
import EdamamImg from '../EdamamImg.png';
import TastyImg from '../TastyImg.png';
import PostgreSQLImg from '../PostgreSQLImg.png';
import FlaskImg from '../FlaskImg.png';
import ReactImg from '../ReactImg.png';
import ReactBootstrapImg from '../ReactBootstrapImg.png';
import KubernetesImg from '../KubernetesImg.png';
import GCPImg from '../GCPImg.png';


function generateUsernameToRealNames() { // creates reverse mapping
  let unameToRealNames = {}
  for (var key in users) {
    for (var username of users[key]["usernames"]) {
      unameToRealNames[username] = key
    }
  }
  return unameToRealNames
}

function calculateTotalCommits(commitData) {
  let total = 0
  for (var key in users) {
    for (var username of users[key]["usernames"]) {
      total += username in commitData ? commitData[username].length : 0
    }
  }
  return total
}

function calculateTotalTests() {
  let total = 0
  for (var key in users) {
    total += users[key]["tests"]
  }
  return total
}

function calculateTotalIssues(issuesData) {
  let total = 0
  for (var key in users) {
    for (var username of users[key]["usernames"]) {
      total += username in issuesData ? (issuesData[username].opened + issuesData[username].closed) : 0
    }
  }
  return total
}

var users = {
  "Finn Frankis": {"usernames": ["FinnitoProductions", "Finn Frankis"], "tests": 15, "bio": "Finn is a junior CS major at UT. In his free time, he loves to run, hike, and adventure.", "responsibilities": "Front-end development, frontend unit testing, Phase 1 Team Leader"},
  "Alex Burton": {"usernames": ["AMACB", "Alex Burton"], "tests": 10, "bio": "Alex is a current junior interested in CS-related theory and cryptography. Outside of school, he enjoys playing electric guitar, eating and cooking Taiwanese food, and attending EDM concerts.", "responsibilities": "Devops (setting up Gitlab, CI, GCP/docker/k8s), PostgreSQL, API scraping scripts/data, Backend unit testing"},
  "Lauren Kim": {"usernames": ["kimlaurend94", "Lauren Kim"], "tests": 6, "bio": "Lauren is a junior CS major and enjoys traveling, trying new restaurants in Austin, and playing video games. Within CS, she is still exploring different areas but has interests in back-end development and AI.", "responsibilities": "Postman API Documentation & testing, User stories, Backend development, Technical report"},
  "Brice Chen": {"usernames": ["Tomruler", "Brice Chen"], "tests": 0, "bio": "Brice is a senior CS major who enjoys reading in his free time. His main focus this year is finding a job, like many seniors, but he's hopeful given his degree at UT.", "responsibilities": "Online API research, Makefile and custom CI files, Frontend Design/SCSS, Managing the discord server and customer/developer group communication"},
}
var usernameToRealNames = generateUsernameToRealNames()

function commitDataToCards(commitData, issuesData) {
  var retList = []
  var i = 1;
  for (var username in commitData) {
    retList.push( <Card style={{ width: '18rem' }}>
    <Card.Img variant="top" src={`${username}.jpeg`} />
    <Card.Body>
      <Card.Title>{username}</Card.Title>
      <Card.Text>
      <ul class="list-group">
      <li class="list-group-item"> <b>Bio</b> {users[username]["bio"]} </li>
      <li class="list-group-item"> <b>Major Responsibilities</b> {users[username]["responsibilities"]} </li>
      <li class="list-group-item"> <b> Total Commits </b> {commitData[username].length} </li>
      <li class="list-group-item"> <b>Total Issues</b> {issuesData[username] !== undefined ? issuesData[username]["all"] : 0} </li>
      <li class="list-group-item"> <b>Unit Tests</b> {users[username]["tests"]} </li>
      </ul>
      </Card.Text>
    </Card.Body>
  </Card>)
    i++;
  }
  return retList
}

const About = () => {
  const [commitData, setCommitData] = useState({})
  const [issuesData, setIssuesData] = useState({})
  const [visitedUsernames, setVisitedUsernames] = useState({})
  
  useEffect(() => {
    const commitsEndpoint = "https://gitlab.com/api/v4/projects/39685225/repository/commits?per_page=100000"

    const fetchCommitData = () => {
        try {
          fetch(commitsEndpoint)
            .then((response) => response.json())
            .then((data) => {
              var usernamesList = []
              var categorizedData = {}
              for (let i = 0; i < data.length; i++) {
                if (!(data[i].author_name in usernameToRealNames)) {
                  continue
                }
                let fullName = usernameToRealNames[data[i].author_name]
                if (categorizedData[fullName] === undefined) {
                  categorizedData[fullName] = []
                }
                if (!usernamesList.includes(fullName))
                  usernamesList.push(fullName)
                categorizedData[fullName].push(data[i])
              }
              
              setCommitData(categorizedData)
            })
        } catch (error) {
            console.log("error", error);
        }
    };
    fetchCommitData();  
  }, []);

  useEffect(() => {
    const fetchIssuesData = () => {
        for(let username in usernameToRealNames) {
          try {
            console.log(visitedUsernames)
            if (visitedUsernames[username] !== undefined && visitedUsernames[username])
              continue
            fetch(`https://gitlab.com/api/v4/projects/39685225/issues_statistics?author_username=${username}`)
              .then((response) => response.json())
              .then((data) => {
                if (visitedUsernames[username]) // this extra check shouldn't be necessary, but it is for some reason
                  return
                var categorizedData = issuesData
                let fullName = usernameToRealNames[username]

                if (!(fullName in categorizedData)) {
                  categorizedData[fullName] = data["statistics"]["counts"]
                } else {
                  categorizedData[fullName]["all"] += data["statistics"]["counts"]["all"]
                  categorizedData[fullName]["closed"] += data["statistics"]["counts"]["closed"]
                  categorizedData[fullName]["opened"] += data["statistics"]["counts"]["opened"]
                }

                let allVisitedUsernames = visitedUsernames
                allVisitedUsernames[username] = true
                setVisitedUsernames(visitedUsernames)            
                setIssuesData(categorizedData)
              },)
        } catch (error) {
            console.log("error", error);
        }
      }
    };
    
    fetchIssuesData();
  }, [])

  const Tool = (props) => {
    return (
        <Card style = {{ width: '15rem'}}>
            <Card.Body className="text">
                <Card.Img variant="top" src={props.tool["image"]}/>
                <a href={props.tool["link"]}>
                    <Card.Title>{props.tool["name"]}</Card.Title>
                </a>
                <Card.Text>{props.tool["description"]}</Card.Text>
            </Card.Body>
        </Card>
    )
  }

  const tools = [
    {
        "name": "PostgreSQL",
        "image": PostgreSQLImg,
        "link": "https://www.postgresql.org/",
        "description": "We used PostgreSQL as our backend database both in our local and production environments.",
    },
    {
        "name": "React",
        "image": ReactImg,
        "link": "https://reactjs.org/",
        "description": "We used React, the powerful declarative Javascript framework, to drive the state and view management of the front-end. ",
    },
    {
        "name": "React Bootstrap",
        "image": ReactBootstrapImg,
        "link": "https://react-bootstrap.github.io/",
        "description": "We used Bootstrap to drive the CSS of the front-end.",
    },
    {
        "name": "Google Cloud Platform (GCP)",
        "image": GCPImg,
        "link": "https://console.cloud.google.com/getting-started",
        "description": "A cloud platform that supports Kubernetes that actually hosts our application.",
    },
    {
        "name": "Kubernetes",
        "image": KubernetesImg,
        "link": "https://kubernetes.io/",
        "description": "An open source container orchestration technology used to manage our application.",
    },
    {
        "name": "Flask",
        "image": FlaskImg,
        "link": "https://flask.palletsprojects.com/en/2.2.x/",
        "description": "We used Flask and SQLAlchemy to implement our backend API and interact with the database.",
    },
  ]

  const apis = [
    {
        "name": "GitLab API",
        "image": GitLabImg,
        "link": "https://docs.gitlab.com/ee/api/",
        "description": "We used the GitLab API to track team stats on the repo.",
    },
    {
        "name": "Google Maps API",
        "image": GoogleMapsImg,
        "link": "https://developers.google.com/maps",
        "description": "We used the Google Maps API to provide locations of our grocery stores",
    },
    {
        "name": "Tasty API",
        "image": TastyImg,
        "link": "https://rapidapi.com/apidojo/api/tasty",
        "description": "We used the Tasty API for recipe data.",
    },
    {
        "name": "Edamam API",
        "image": EdamamImg,
        "link": "https://api.edamam.com/",
        "description": "We used the Edamam API for ingredient and nutrition data.",

    },
  ]


  return (
    <div className={general_css.background}>
      <h1> What We Do </h1>
      <Container>
      <p> With so much data available at a user’s fingertips, many recipes from cultures across the world can be unlocked. Missing a few ingredients for your newly found exotic cuisine? Just hop on the stores section and find the nearest high rated one to you. </p>
      </Container>
      <h1> Data Integration </h1>
      <Container>
      <p> Meal Maker is a website that provides quick and easy access to information on recipes, ingredients, and stores. This would allow the user to determine what meals they can make with the ingredients and equipment they have on hand, and what ingredients they need to purchase to cook the meal they want and where to buy them. 
      With this data, we hope to inspire people to explore the realm of cooking and feel that preparing a meal is a hassle-free experience. </p>
      </Container>
      <h1> Members </h1>
      <div className="card-deck">
        <div className={general_css.wrapping_elements}>
        {commitDataToCards(commitData, issuesData)}
        </div>
      </div>
      <div className={general_css.aboutInfo}>
      <h1>Team Stat Totals</h1>
        <Card className={general_css.aboutInfo}>
            <Card.Body>
                <Card.Text><b>Total Commits: </b> {calculateTotalCommits(commitData)}</Card.Text>
                <Card.Text><b>Total Issues: </b> {calculateTotalIssues(issuesData)}</Card.Text>
                <Card.Text><b>Total Unit Tests: </b> {calculateTotalTests()} </Card.Text>
            </Card.Body>
        </Card>
      </div>
      <div className={general_css.aboutInfo}>
      <h1 className="text">APIs Scraped</h1>
          <Table className={general_css.aboutTable}>
              <tbody className='cards'>
                  <tr>
                      {apis.map(api => (
                          <td key={api.name}><Tool tool={api}/></td>
                      ))}
                  </tr>
              </tbody>
          </Table>
      </div>
      <div className={general_css.aboutInfo}>
      <h1> Tools </h1>
          <Table className={general_css.aboutTable}>
              <tbody className='cards'>
                  <tr>
                  {tools.map(tool => (
                      <td key={tool.name}><Tool tool={tool}/></td>
                  ))}
                  </tr>                    
              </tbody>
          </Table>
      </div>
      <div className={general_css.aboutInfo}>
          <h1> Project URLS </h1>
          <Button href="https://gitlab.com/kimlaurend94/cs373-idb">GitLab Project Repository</Button>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <Button href="https://documenter.getpostman.com/view/23542695/2s83tFHXMw">Postman API Documentation</Button>
      </div>
    </div>
  );
};
  
export default About;
