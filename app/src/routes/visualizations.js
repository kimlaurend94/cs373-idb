import React, { useEffect, useState } from "react";
import * as d3 from "d3";
import Card from 'react-bootstrap/Card';
import { ScatterChart, Scatter, PieChart, Pie, BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Label } from 'recharts';

import general_css from '../css/site_sass.module.css'

const COLORS = ['#00C49F', '#FFBB28', '#FF8042'];

const Visualizations = () => {
    const [storeVisualData, setStoreVisualData] = useState()
    const [ingredientVisualData, setIngredientVisualData] = useState()
    const [recipesVisualData, setRecipesVisualData] = useState()

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/stores`).then(
            resp => resp.json()
        ).then(
            data => {
                let b0 = data.page.filter(x => (0 <= x.rating && x.rating < 1)).length;
                let b1 = data.page.filter(x => (1 <= x.rating && x.rating < 2)).length;
                let b2 = data.page.filter(x => (2 <= x.rating && x.rating < 3)).length;
                let b3 = data.page.filter(x => (3 <= x.rating && x.rating < 4)).length;
                let b4 = data.page.filter(x => (4 <= x.rating && x.rating <= 5)).length;
                setStoreVisualData(
                   [
                    {count: b0, name: "0-1"},
                    {count: b1, name: "1-2"},
                    {count: b2, name: "2-3"},
                    {count: b3, name: "3-4"},
                    {count: b4, name: "4-5"},
                   ]);
            }
        )
    }, [])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/ingredients`).then(
            resp => resp.json()
        ).then(
            data => {
                let bp = data.page.filter(x => x.protein > x.carbs && x.protein > x.fat).length;
                let bf = data.page.filter(x => x.fat > x.carbs && x.fat > x.protein).length;
                let bc = data.page.filter(x => x.carbs > x.protein && x.carbs > x.fat).length;
                setIngredientVisualData(
                    [{"name": "Protein", value: bp},{"name": "Carbs", value: bc},{"name": "Fat", value: bf}]
                )
            }
        )
    }, [])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/recipes`).then(
            resp => resp.json()
        ).then(
            data => {
                setRecipesVisualData(data.page.map(x => {
                    let obj = {"time": x.total_time, "ingredients": x.ingredients.length}
                    return obj
                }).filter(x => x.time < 1000));
            }
        )
    }, [])

    return (
    <div className={general_css.background}>
        <h1>Visualizations</h1>

        <h2>Stores</h2>
        <div className="d-flex align-items-center justify-content-center">
        <BarChart
          width={600}
          height={400}
          data={storeVisualData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Bar dataKey="count" fill="#8884d8" />
        </BarChart>
        </div>
        <div className="d-flex align-items-center justify-content-center">
        <p>Number of stores vs. Rating</p>
        </div>

        <h2>Ingredients</h2>
        <div className="d-flex align-items-center justify-content-center">
        <PieChart
          width={600}
          height={400}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}>
            <Pie
              dataKey="value"
              isAnimationActive={false}
              data={ingredientVisualData}
              cx="50%"
              cy="50%"
              outerRadius={160}
              fill="#8884d8"
              label={x => x.name}>
              {[0,0,0].map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                ))}
            </Pie>

            <Tooltip />
        </PieChart>
        </div>
        <div className="d-flex align-items-center justify-content-center">
        <p>Number of ingredients by primary macronutrient</p>
        </div>

        <h2>Recipes</h2>
        <div className="d-flex align-items-center justify-content-center">
        <ScatterChart
          width={600}
          height={400}
          data={recipesVisualData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid />
          <XAxis type="number" dataKey="ingredients" name="Ingredients" />
          <YAxis type="number" dataKey="time" name="Time" unit="mins"/>
          <Tooltip />
          <Scatter data={recipesVisualData} fill="#8884d8" />
        </ScatterChart>
        </div>
        <div className="d-flex align-items-center justify-content-center">
        <p>Recipe time vs. Number of ingredients</p>
        </div>
    </div>
    );
};

export default Visualizations;