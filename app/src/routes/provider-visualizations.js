import React, { useEffect, useState } from "react";
import * as d3 from "d3";
import Card from 'react-bootstrap/Card';
import { ScatterChart, Scatter, PieChart, Pie, BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Label } from 'recharts';

import general_css from '../css/site_sass.module.css'

const COLORS = ['#00C49F', '#FFBB28', '#FF8042'];

const ProviderVisualizations = () => {
    const [coffeeShopVisualData, setCoffeeShopVisualData] = useState()
    const [libraryVisualData, setLibraryVisualData] = useState()
    const [universityVisualData, setUniversityVisualData] = useState()

    useEffect(() => {
        fetch("https://api.studyspots.me/coffeeshops?per_page=100").then(
            resp => resp.json()
        ).then(
            data => {
                var counts = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0}
                for (var i in data.results) {
                    var x = data.results[i]
                    let amt = (x.hours_day_0_closed !== "-1") + (x.hours_day_1_closed !== "-1") + (x.hours_day_2_closed !== "-1") + (x.hours_day_3_closed !== "-1") + (x.hours_day_4_closed !== "-1") + (x.hours_day_5_closed !== "-1") + (x.hours_day_6_closed !== "-1")
                    counts[amt] += 1
                }
                var ret = [{"name": "7 days", count: counts[7]}, {"name": "6 days", count: counts[6]}, {"name": "5 or fewer days", count: counts[5] + counts[4] + counts[3] + counts[2] + counts[1] + counts[0]}]
//                var ret = []
//                for (var k in counts) {
//                    ret.push({"name": "" + k, "count": counts[k]})
//                }
//                console.log(ret)
                setCoffeeShopVisualData(ret)
            }
        )
    }, []);

    useEffect(() => {
        fetch("https://api.studyspots.me/libraries?per_page=100&page=6").then(
            resp => resp.json()
        ).then(
            data => {
                let b0 = data.results.filter(x => (0 <= x.rating && x.rating < 1)).length;
                let b1 = data.results.filter(x => (1 <= x.rating && x.rating < 2)).length;
                let b2 = data.results.filter(x => (2 <= x.rating && x.rating < 3)).length;
                let b3 = data.results.filter(x => (3 <= x.rating && x.rating < 4)).length;
                let b4 = data.results.filter(x => (4 <= x.rating && x.rating <= 5)).length;
                setLibraryVisualData(
                   [
                    {count: b0, name: "0-1"},
                    {count: b1, name: "1-2"},
                    {count: b2, name: "2-3"},
                    {count: b3, name: "3-4"},
                    {count: b4, name: "4-5"},
                   ]);
            }
        )
    }, []);

    useEffect(() => {
        fetch("https://api.studyspots.me/universities?per_page=100").then(
            resp => resp.json()
        ).then(
            data => {
                setUniversityVisualData(data.results.map(x => {
                    let ret = {"instate": x.instate_tuition, "outstate": x.outstate_tuition}
                    return ret
                }))
            }
        )
    }, []);

    return (
    <div className={general_css.background}>
        <h1>Provider Visualizations</h1>

        <h2>Coffee Shops</h2>
        <div className="d-flex align-items-center justify-content-center">
        <PieChart
          width={600}
          height={400}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}>
            <Pie
              dataKey="count"
              isAnimationActive={false}
              data={coffeeShopVisualData}
              cx="50%"
              cy="50%"
              outerRadius={160}
              fill="#8884d8"
              label={x => "" + x.name}>
                {[0,0,0].map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                  ))}
            </Pie>

            <Tooltip />
        </PieChart>
        </div>
        <div className="d-flex align-items-center justify-content-center">
        <p>Coffee shops by days open per week (sample size: 100)</p>
        </div>

        <h2>Libraries</h2>
        <div className="d-flex align-items-center justify-content-center">
        <BarChart
          width={600}
          height={400}
          data={libraryVisualData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Bar dataKey="count" fill="#8884d8" />
        </BarChart>
        </div>
        <div className="d-flex align-items-center justify-content-center">
        <p>Number of libraries vs. Rating (sample size: 100)</p>
        </div>

        <h2>Universities</h2>
        <div className="d-flex align-items-center justify-content-center">
        <ScatterChart
          width={600}
          height={400}
          data={universityVisualData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid />
          <XAxis type="number" dataKey="instate" name="In State Tuition"/>
          <YAxis type="number" dataKey="outstate" name="Out of State Tuition"/>
          <Tooltip />
          <Scatter data={universityVisualData} fill="#8884d8" />
        </ScatterChart>
        </div>
        <div className="d-flex align-items-center justify-content-center">
        <p>In state tuition vs. Out of state tutition (sample size: 100)</p>
        </div>
    </div>
    );
};

export default ProviderVisualizations;