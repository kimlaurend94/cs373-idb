from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import unittest

HOME_PAGE_URL = "localhost:3000"
TOTAL_RESULTS_STR = "Total results:"

class TestMealMakerGui(unittest.TestCase):
    @staticmethod
    def get_page_source(url):
        chrome_options = Options()
        driver = webdriver.Chrome(chrome_options=chrome_options)
        driver.get(url)
        return BeautifulSoup(driver.page_source, 'html.parser')

    # Tests the welcome heading for the homepage.
    def test_home_page_heading(self):
        soup = TestMealMakerGui.get_page_source(HOME_PAGE_URL)
        headings = soup.find('h1')

        self.assertEqual(len(headings), 1)
        self.assertEqual(next(iter(headings)).text, "Welcome to MealMaker!")

    # Tests the welcome paragraph for the homepage.
    def test_home_page_paragraph(self):
        soup = TestMealMakerGui.get_page_source(HOME_PAGE_URL)
        paragraphs = soup.find('p')

        self.assertEqual(len(paragraphs), 1)
        self.assertEqual(next(iter(paragraphs)).text, "We encourage you to take a look at our grocery stores, ingredients, and recipes to explore nutritious and delicious food options.")

    # Test that the recipes table count is present.
    def test_recipes_table_count(self):
        soup = TestMealMakerGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "recipes"))
        bolds = soup.find('b')
        *_, last_bold = iter(bolds)

        self.assertIn(TOTAL_RESULTS_STR, last_bold.text)

    # Test that the ingredients table count is present.
    def test_ingredients_table_count(self):
        soup = TestMealMakerGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "ingredients"))
        bolds = soup.find('b')
        *_, last_bold = iter(bolds)

        self.assertIn(TOTAL_RESULTS_STR, last_bold.text)

    # Test that the stores table count is present.
    def test_stores_table_count(self):
        soup = TestMealMakerGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "stores"))
        bolds = soup.find('b')
        *_, last_bold = iter(bolds)

        self.assertIn(TOTAL_RESULTS_STR, last_bold.text)
    
    # Test that all recipes table headings are present.
    def test_recipes_table_headings(self):
        soup = TestMealMakerGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "recipes"))
        bolds = soup.find('b')
        bolds_iter = iter(bolds)

        for val in ["Recipe Name", "Prep Time (mins)", "Cook Time (mins)", "Serving Size (people)", "Occasion"]:
            next_bold = next(bolds_iter)
            self.assertEqual(next_bold.text, val)

    # Test that all ingredients table headings are present.
    def test_ingredients_table_headings(self):
        soup = TestMealMakerGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "ingredients"))
        bolds = soup.find('b')
        bolds_iter = iter(bolds)

        for val in ["Ingredient Name", "Calories (kcal)", "	Protein (g)", "Carbohydrates (g)", "Fiber (g)"]:
            next_bold = next(bolds_iter)
            self.assertEqual(next_bold.text, val)

    # Test that all stores table headings are present.
    def test_stores_table_headings(self):
        soup = TestMealMakerGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "stores"))
        bolds = soup.find('b')
        bolds_iter = iter(bolds)

        for val in ["Store Name", "Rating (stars)", "Phone Number", "City", "Delivery?"]:
            next_bold = next(bolds_iter)
            self.assertEqual(next_bold.text, val)

    # Test that each recipe ends with "Enjoy!".
    def test_recipe_instance(self):
        soup = TestMealMakerGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "recipes/recipe6"))
        l = soup.find('ul')
        rs = l.findAll('li')

        *_, last_entry = iter(rs)
        self.assertEqual(last_entry.text, "Enjoy!")

    # Test that legend is rendered correctly.
    def test_ingredient_legend(self):
        soup = TestMealMakerGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "ingredients/ingredient48"))
        paragraphs = soup.find('p')

        legend_keys = {"Fat": False, "Protein": False, "Carbohydrates": False, "Fiber": False}
        for paragraph in paragraphs:
            if paragraph.text in legend_keys:
                legend_keys[paragraph.text] = True
        
        for key, val in legend_keys.items():
            self.assertEqual(val, True)





    
