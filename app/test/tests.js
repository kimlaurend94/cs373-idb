import {generateHtmlDataPointFromValue, generateHtmlBooleanFromValue, getStoreNameFromId, getRecipeNameFromId, getIngredientNameFromId} from "../src/utils.js"
import assert from 'assert'

describe('generateHtmlDataPointFromValue', function () {
    it('should return "15 kg" for value "15" and units "kg"', function () {
      assert.equal(generateHtmlDataPointFromValue(15, "kg"), "15 kg")
    });
});

describe('generateHtmlDataPointFromValue', function () {
    it('should return N/A for null value', function () {
      assert.equal(generateHtmlDataPointFromValue(null, "kg"), "N/A")
    });
});

describe('generateHtmlDataPointFromValue', function () {
    it('should return "2" for value "2" and empty units', function () {
      assert.equal(generateHtmlDataPointFromValue(2, ""), "2")
    });
});

describe('generateHtmlBooleanFromValue', function () {
    it('should return "Yes" for value "true"', function () {
      assert.equal(generateHtmlBooleanFromValue(true), "Yes")
    });
});

describe('generateHtmlBooleanFromValue', function () {
    it('should return "No" for value "false"', function () {
      assert.equal(generateHtmlBooleanFromValue(false), "No")
    });
});

describe('generateHtmlBooleanFromValue', function () {
    it('should return "N/A" for value "null"', function () {
      assert.equal(generateHtmlBooleanFromValue(null), "N/A")
    });
});

describe('getStoreNameFromId', function() {
    it('should return "No Name Provided" for empty stores list', function() {
        assert.equal(getStoreNameFromId([], "anything"), "No Name Provided")
    });
});

describe('getStoreNameFromId', function() {
    it('should return correct name for nonempty stores list if match is found', function() {
        assert.equal(getStoreNameFromId([{"grocery_store_id": 2, "name": "Store1"}, {"grocery_store_id": 9, "name": "Store2"}], 9), "Store2")
    });
});

describe('getStoreNameFromId', function() {
    it('should return "No Name Provided" for nonempty stores list if match is not found', function() {
        assert.equal(getStoreNameFromId([{"grocery_store_id": 2, "name": "Store1"}, {"grocery_store_id": 9, "name": "Store2"}], 7), "No Name Provided")
    });
});

describe('getRecipeNameFromId', function() {
    it('should return "No Name Provided" for empty recipes list', function() {
        assert.equal(getRecipeNameFromId([], "anything"), "No Name Provided")
    });
});

describe('getRecipeNameFromId', function() {
    it('should return correct name for nonempty recipes list if match is found', function() {
        assert.equal(getRecipeNameFromId([{"recipe_id": 3, "name": "Recipe1"}, {"recipe_id": 9, "name": "Recipe2"}], 3), "Recipe1")
    });
});

describe('getRecipeNameFromId', function() {
    it('should return "No Name Provided" for nonempty recipes list if match is not found', function() {
        assert.equal(getRecipeNameFromId([{"recipe_id": 3, "name": "Recipe1"}, {"recipe_id": 9, "name": "Recipe2"}], 6), "No Name Provided")
    });
});

describe('getIngredientNameFromId', function() {
    it('should return "No Name Provided" for empty ingredients list', function() {
        assert.equal(getIngredientNameFromId([], "anything"), "No Name Provided")
    });
});

describe('getIngredientNameFromId', function() {
    it('should return correct name for nonempty ingredients list if match is found', function() {
        assert.equal(getIngredientNameFromId([{"ingredient_id": 51, "name": "Ingredient1"}, {"ingredient_id": 42, "name": "Ingredient2"}], 42), "Ingredient2")
    });
});

describe('getIngredientNameFromId', function() {
    it('should return "No Name Provided" for nonempty ingredients list if match is not found', function() {
        assert.equal(getIngredientNameFromId([{"ingredient_id": 51, "name": "Ingredient1"}, {"ingredient_id": 42, "name": "Ingredient2"}], 68), "No Name Provided")
    });
});