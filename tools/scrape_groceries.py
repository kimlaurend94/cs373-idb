#!/usr/bin/env python

import json
import requests

with open("keys.secret", "r") as f:
    keys = json.load(f)

coords = [
    "29.763 -95.363",   # houston
    "29.424 -98.494",   # san antonio
    "32.783 -96.807",   # dallas
    "30.267 -97.743",   # austin
    "32.725 -97.321",   # fort worth
    "31.759 -106.487",  # el paso
    "32.736 -97.108",   # arlington
    "27.801 -97.396",   # corpus christi
    "33.02 -96.699",    # plano
    "33.574 -101.871",  # lubbock
    "31.551 -97.150",   # waco
    "30.628 -96.335",   # college station
    "29.786 -95.824",   # katy
    "29.883 -97.941"    # san marcos
]

grocery_stores = []

for c in coords:
    r = requests.get("https://maps.googleapis.com/maps/api/place/nearbysearch/json",
                     params={"location": c,
                             "radius": "50000",
                             "type": "supermarket",
                             "key": keys["google-maps"]})

    grocery_stores += json.loads(r.text)["results"]

json.dump(grocery_stores, open("grocery_stores.json", "w"), indent=2)