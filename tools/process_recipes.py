#!/usr/bin/env python

import json
import requests
import traceback

with open("keys.secret", "r") as f:
    keys = json.load(f)

recipes = json.load(open("recipes_processed.json", "r"))
ingredients = json.load(open("ingredients_processed_images.json"))

ing_to_labels = {}

for i in ingredients:
    ing_to_labels[i["name"]] = i["health_labels"][:]

for r in recipes:
    names = [x for x in r["ingredients_names"] if x is not None]
    if len(names) == 0:
        health_labels = []
    else:
        health_labels = ing_to_labels[names[0]]
        for i_name in names:
            health_labels = [x for x in health_labels if x in ing_to_labels[i_name]]

    r["health_labels"] = health_labels

json.dump(recipes, open("recipes_processed_labels.json", "w"), indent=2)