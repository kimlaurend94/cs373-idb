# API Scraping Workflow

## Groceries
1. `scrape_groceries.py` will put raw data into `grocery_stores.json`.
2. `process_groceries.py` will convert the raw data from `grocery_stores.json` into `grocery_stores_processed.json`.

## Recipes / Ingredients
1. `scrape_recipes.py` will put raw data into `recipes.json`.
2. `scrape_ingredients.py` will take the recipes from `recipes.json` and put raw ingredient data (from the recipes) into `recipes_processed.json`
3. `scrape_ingredients_again.py` will take `recipes_processed.json`, and scrapes to output `ingredients_processed.json`.