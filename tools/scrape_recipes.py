#!/usr/bin/env python

import json
import requests
import sys
import traceback
import unicodedata

with open("keys.secret", "r") as f:
    keys = json.load(f)

def process_ingredient(i):
    quantity = unicodedata.normalize("NFKD", i["measurements"][0]["quantity"]).replace("⁄", "/")
    if quantity == "0":
        return i["ingredient"]["name"]
    elif i["measurements"][0]["unit"]["display_plural"] == "":
        return quantity + " " + i["ingredient"]["name"]
    else:
        return quantity + " " + i["measurements"][0]["unit"]["display_plural"] + " " + i["ingredient"]["name"]

def process_recipe(recipe_raw):
    recipe = {}
    recipe["name"] = recipe_raw["name"]
    recipe["description"] = recipe_raw["description"]
    if len(recipe["description"]) == 0:
        raise ValueError("no description")
    recipe["credits"] = ", ".join([x["name"] for x in recipe_raw["credits"]])
    recipe["prep_time"] = recipe_raw["prep_time_minutes"]
    recipe["cook_time"] = recipe_raw["cook_time_minutes"]
    recipe["total_time"] = recipe_raw["total_time_minutes"]
    recipe["num_servings"] = recipe_raw["num_servings"]
    recipe["ingredients"] = [process_ingredient(y) for x in recipe_raw["sections"] for y in x["components"]]
    recipe["instructions"] = [x["display_text"] for x in recipe_raw["instructions"]]
    recipe["equipment"] = [x["name"] for x in recipe_raw["tags"] if x["type"] == "equipment"]
    recipe["occasion"] = [x["name"] for x in recipe_raw["tags"] if x["type"] in ["occasion", "holiday", "meal"]]
    recipe["cuisine"] = [x["name"] for x in recipe_raw["tags"] if x["type"] == "cuisine"]
    recipe["video_url"] = recipe_raw["renditions"][0]["url"]
    recipe["thumbnail_url"] = recipe_raw["thumbnail_url"]
    return recipe


processed = []

for i in range(20):
    r = requests.get("https://tasty.p.rapidapi.com/recipes/list",
                     headers={"X-RapidAPI-Key": keys["recipes-key"],
                              "X-RapidAPI-Host": keys["recipes-host"]},
                     params={"from": f"{6 + 20*i}", "size": "20"})

    data = json.loads(r.text)
    assert "results" in data

    for r in data["results"]:
        try:
            processed.append(process_recipe(r))
        except Exception as e:
            print("couldnt process:")
            print(data)
            print(traceback.format_exc())

print(f"processed {len(processed)} recipes")
with open("recipes.json", "w") as f:
    json.dump(processed, f, indent=2)
