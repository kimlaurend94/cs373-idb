#!/usr/bin/env python

import json
import requests
import sys
import traceback
import unicodedata
import time

with open("keys.secret", "r") as f:
    keys = json.load(f)


def get_image_urls(q, count=10):
    r = requests.get("https://bing-image-search1.p.rapidapi.com/images/search",
                     headers={"X-RapidAPI-Key": keys["bing-key"],
                              "X-RapidAPI-Host": keys["bing-host"]},
                     params={"q": q, "mkt": "en-US", "count": count})

    data = json.loads(r.text)

    return [x["contentUrl"] for x in data["value"]]


ingredients = json.load(open("ingredients_processed.json"))
for i in ingredients:
    try:
        i["image_urls"] = get_image_urls(i["name"])
        print(f"added image_urls for {i['name']}")
        print(i["image_urls"])
    except Exception:
        print(f"couldnt process for name {i['name']}")
        traceback.print_exc()

json.dump(ingredients, open("ingredients_processed_images.json", "w"), indent=2)


grocery_stores = json.load(open("grocery_stores_processed.json"))
for g in grocery_stores:
    try:
        g["image_urls"] = get_image_urls(g["name"])
        print(f"added image_urls for {g['name']}")
        print(g["image_urls"])
    except Exception:
        print(f"couldnt process for name {g['name']}")
        traceback.print_exc()

json.dump(grocery_stores, open("grocery_stores_processed_images.json", "w"), indent=2)
