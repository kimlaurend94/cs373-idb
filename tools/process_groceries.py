#!/usr/bin/env python

import json
import requests
import traceback

with open("keys.secret", "r") as f:
    keys = json.load(f)

raw = json.load(open("grocery_stores.json", "r"))
processed = []
seen_pids = set([])

def process_grocery_store(d):
    d = d["result"]
    grocery_store = {}
    grocery_store["name"] = d["name"]
    # grocery_store["neighborhood"] = [x["long_name"] for x in d["address_components"] if "neighborhood" in x["types"]][0]
    grocery_store["city"] = [x["long_name"] for x in d["address_components"] if "locality" in x["types"]][0]
    grocery_store["latitude"] = d["geometry"]["location"]["lat"]
    grocery_store["longitude"] = d["geometry"]["location"]["lng"]
    grocery_store["address"] = d["formatted_address"]
    grocery_store["phone"] = d.get("formatted_phone_number", None)
    grocery_store["has_curbside_pickup"] = d.get("curbside_pickup", None)
    grocery_store["has_delivery"] = d.get("delivery", None)
    grocery_store["opening_hours"] = d["opening_hours"]["weekday_text"]
    grocery_store["price"] = d.get("price_level", None)
    grocery_store["rating"] = d.get("rating", None)
    grocery_store["num_ratings"] = d.get("user_rating_total", None)
    grocery_store["website"] = d.get("website", None)
    grocery_store["maps_url"] = d["url"]

    return grocery_store

for r in raw:
    pid = r["place_id"]
    if pid in seen_pids:
        continue
    else:
        seen_pids.add(pid)
    r = requests.get("https://maps.googleapis.com/maps/api/place/details/json",
                     params={"place_id": pid,
                             "key": keys["google-maps"]})

    data = json.loads(r.text)
    try:
        grocery_store = process_grocery_store(data)
        processed.append(grocery_store)
    except Exception as e:
        print("couldnt process:")
        print(data)
        print(traceback.format_exc())


print(f"saved {len(processed)} grocery stores")
json.dump(processed, open("grocery_stores_processed.json", "w"), indent=2)