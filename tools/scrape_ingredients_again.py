#!/usr/bin/env python

import json
import requests
import traceback

with open("keys.secret", "r") as f:
    keys = json.load(f)

recipes = json.load(open("recipes_processed.json", "r"))

FOOD_APP_ID = keys["ingredients-app-id"]
FOOD_APP_KEY = keys["ingredients-app-key"]
NUT_APP_ID = keys["nutrition-app-id"]
NUT_APP_KEY = keys["nutrition-app-key"]

ingredient_names = list(set([x for r in recipes for x in r["ingredients_names"]]))

ingredients = []


def add_nutrient(p, d, name, lookup_name, wt):
    if lookup_name in d["totalNutrients"] and "quantity" in d["totalNutrients"][lookup_name]:
        p[name] = float(d["totalNutrients"][lookup_name]["quantity"]) / float(wt)
    else:
        p[name] = 0.0


for i in ingredient_names:
    r2 = requests.get("https://api.edamam.com/api/nutrition-data",
                  params={"nutrition-type": "logging",
                          "app_id": NUT_APP_ID,
                          "app_key": NUT_APP_KEY,
                          "ingr": i})

    data = json.loads(r2.text)

    processed = {}
    processed["name"] = i
    processed["diet_labels"] = data.get("dietLabels", [])
    processed["health_labels"] = data.get("healthLabels", [])
    processed["cautions"] = data.get("cautions", [])
    if "totalNutrients" in data and "totalWeight" in data:
        wt = data["totalWeight"]
        add_nutrient(processed, data, "calories", "ENERC_KCAL", wt)
        add_nutrient(processed, data, "protein", "PROCNT", wt)
        add_nutrient(processed, data, "fat", "FAT", wt)
        add_nutrient(processed, data, "saturated_fat", "FASAT", wt)
        add_nutrient(processed, data, "trans_fat", "FATRN", wt)
        add_nutrient(processed, data, "carbs", "CHOCDF", wt)
        add_nutrient(processed, data, "fiber", "FIBTG", wt)
        add_nutrient(processed, data, "sugar", "SUGAR", wt)
        add_nutrient(processed, data, "added_sugar", "SUGAR.added", wt)
        add_nutrient(processed, data, "cholesterol", "CHOLE", wt)

    ingredients.append(processed)


print(f"scraped {len(ingredients)} ingredients")
with open("ingredients_processed.json", "w") as f:
    json.dump(ingredients, f, indent=2)
