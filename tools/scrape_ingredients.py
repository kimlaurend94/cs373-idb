#!/usr/bin/env python

import json
import requests
import traceback

with open("keys.secret", "r") as f:
    keys = json.load(f)

recipes = json.load(open("recipes.json", "r"))

FOOD_APP_ID = keys["ingredients-app-id"]
FOOD_APP_KEY = keys["ingredients-app-key"]
NUT_APP_ID = keys["nutrition-app-id"]
NUT_APP_KEY = keys["nutrition-app-key"]


for recipe in recipes:
    r = requests.post("https://api.edamam.com/api/nutrition-details",
                      params={"app_id": NUT_APP_ID,
                              "app_key": NUT_APP_KEY},
                      json={"ingr": recipe["ingredients"]})

    data = json.loads(r.text)

    recipe["ingredients_weights"] = []
    recipe["ingredients_quantities"] = []
    recipe["ingredients_measures"] = []
    recipe["ingredients_names"] = []
    ingredient_final = {}

    if "ingredients" not in data:
        recipe["ingredients_weights"].append(None)
        recipe["ingredients_measures"].append(None)
        recipe["ingredients_quantities"].append(None)
        recipe["ingredients_names"].append(None)
    else:
        for i_proc in data["ingredients"]:
            if "parsed" not in i_proc or len(i_proc["parsed"]) == 0:
                recipe["ingredients_weights"].append(None)
                recipe["ingredients_measures"].append(None)
                recipe["ingredients_quantities"].append(None)
                recipe["ingredients_names"].append(None)
            else:
                i_proc_p = i_proc["parsed"][0]
                recipe["ingredients_weights"].append(i_proc_p.get("weight", None))
                recipe["ingredients_measures"].append(i_proc_p.get("measure", None))
                recipe["ingredients_quantities"].append(i_proc_p.get("quantity", None))
                recipe["ingredients_names"].append(i_proc_p.get("food", None))


print(f"scraped ingredients from  {len(recipes)} recipes")
with open("recipes_processed.json", "w") as f:
    json.dump(recipes, f, indent=2)

# print(f"processed {len(ingredients)} ingredients")
# with open("ingredients.json", "w") as f:
#     json.dump(ingredients, f, indent=2)


# def process_ingredient(i):
#     r = requests.get("https://api.edamam.com/api/food-database/parser",
#                      params={"nutrition-type": "logging",
#                              "app_id": FOOD_APP_ID,
#                              "app_key": FOOD_APP_KEY,
#                              "ingr": i})
#
#     data1 = json.loads(r.text)["parsed"][0]
#     print(data1)
#
#     exit(0)
#
#     r2 = requests.get("https://api.edamam.com/api/nutrition-data",
#                       params={"nutrition-type": "logging",
#                               "app_id": NUT_APP_ID,
#                               "app_key": NUT_APP_KEY,
#                               "ingr": i})
#
#     data2 = json.loads(r2.text)
#     processed = {}
#     processed["name"] = data["food"]["label"]
#     processed["category"] = data["food"]["category"]
#     processed["unit"] = data["measure"]["label"]
#     processed["diet_labels"] = data2["dietLabels"]
#     processed["health_labels"] = data2["healthLabels"]
#     processed["cautions"] = data2["cautions"]
#     processed["calories"] = data["food"]["nutrients"]["ENERC_KCAL"]
#     processed["protein"] = data["food"]["nutrients"]["PROCNT"]
#     processed["fat"] = data["food"]["nutrients"]["FAT"]
#     processed["carbs"] = data["food"]["nutrients"]["CHOCDF"] / qty
#     processed["fiber"] = data["food"]["nutrients"]["FIBTG"] / qty
#     return processed
#
#
# ingredients = []
# added = set([])
# ingredient_map = {}
#
# for recipe in recipes:
#     for i in recipe["ingredients"]:
#         try:
#             p = process_ingredient(i)
#             print(p)
#             p = process_ingredient(p["name"])
#             print(p)
#             exit(1)
#             ingredient_map[i] = p["name"]
#             if p["name"] not in added:
#                 ingredients.append(p)
#                 added.add(p["name"])
#         except Exception as e:
#             print("couldnt process:")
#             print(i)
#             print(traceback.format_exc())
#
# print(f"processed {len(ingredients)} ingredients")
# with open("ingredients.json", "w") as f:
#     json.dump(ingredients, f, indent=2)
#
