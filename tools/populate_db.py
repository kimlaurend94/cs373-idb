#!/usr/bin/env python

import sqlalchemy
import json
import requests
import traceback

with open("keys.secret", "r") as f:
    keys = json.load(f)

password = keys["postgres"]
user = "user"
host = "localhost"
db = "production"

engine = sqlalchemy.create_engine(f"postgresql://{user}:{password}@{host}:5432/{db}")
engine.connect()

ingredients = json.load(open("ingredients_processed_images.json"))
grocery_stores = json.load(open("grocery_stores_processed_images.json"))
recipes = json.load(open("recipes_processed_labels.json"))
grocery_links = json.load(open("grocery_links.json"))


def text_array(a):
    return f"ARRAY[{','.join(map(text,a))}]::TEXT[]"


def float_array(a):
    return f"ARRAY[{','.join(map(json.dumps,a))}]::FLOAT[]"


def int_array(a):
    return f"ARRAY[{','.join(map(json.dumps,a))}]::INT[]"


def text(a):
    if a is None:
        return "null"
    a = a.replace("'", "''").replace('%', '%%')
    return f"'{a}'"


with engine.begin() as conn:
    for i in ingredients:
        conn.execute(f"""
        INSERT INTO ingredients (
            name,
            diet_labels,
            health_labels,
            cautions,
            calories,
            protein,
            fat,
            saturated_fat,
            trans_fat,
            carbs,
            fiber,
            sugar,
            added_sugar,
            cholesterol,
            image_urls
        )
        VALUES (
            {text(i["name"])},
            {text_array(i["diet_labels"])},
            {text_array(i["health_labels"])},
            {text_array(i["cautions"])},
            {json.dumps(i["calories"])},
            {json.dumps(i["protein"])},
            {json.dumps(i["fat"])},
            {json.dumps(i["saturated_fat"])},
            {json.dumps(i["trans_fat"])},
            {json.dumps(i["carbs"])},
            {json.dumps(i["fiber"])},
            {json.dumps(i["sugar"])},
            {json.dumps(i["added_sugar"])},
            {json.dumps(i["cholesterol"])},
            {text_array(i["image_urls"])}
        )
        """)

    for r in recipes:
        if r["ingredients_names"] == [None]:
            continue
        conn.execute(f"""
        INSERT INTO recipes (
            name,
            description,
            credits,
            prep_time,
            cook_time,
            total_time,
            num_servings,
            ingredients,
            ingredient_weights,
            ingredient_quantities,
            ingredient_measures,
            ingredient_names,
            health_labels,
            instructions,
            equipment,
            occasion,
            cuisine,
            video_url,
            thumbnail_url
        )
        VALUES (
            {text(r["name"])},
            {text(r["description"])},
            {text(r["credits"])},
            {json.dumps(r["prep_time"])},
            {json.dumps(r["cook_time"])},
            {json.dumps(r["total_time"])},
            {json.dumps(r["num_servings"])},
            {text_array(r["ingredients"])},
            {float_array(r["ingredients_weights"])},
            {float_array(r["ingredients_quantities"])},
            {text_array(r["ingredients_measures"])},
            {text_array(r["ingredients_names"])},
            {text_array(r["health_labels"])},
            {text_array(r["instructions"])},
            {text_array(r["equipment"])},
            {text_array(r["occasion"])},
            {text_array(r["cuisine"])},
            {text(r["video_url"])},
            {text(r["thumbnail_url"])}
        );
        """)

    for g in grocery_stores:
        conn.execute(f"""
        INSERT INTO grocery_stores (
            name,
            city,
            latitude,
            longitude,
            address,
            phone,
            has_curbside_pickup,
            has_delivery,
            opening_hours,
            price,
            rating,
            num_ratings,
            website,
            maps_url,
            image_urls
        )
        VALUES (
            {text(g["name"])},
            {text(g["city"])},
            {json.dumps(g["latitude"])},
            {json.dumps(g["longitude"])},
            {text(g["address"])},
            {text(g["phone"])},
            {json.dumps(g["has_curbside_pickup"])},
            {json.dumps(g["has_delivery"])},
            {text_array(g["opening_hours"])},
            {json.dumps(g["price"])},
            {json.dumps(g["rating"])},
            {json.dumps(g["num_ratings"])},
            {text(g["website"])},
            {text(g["maps_url"])},
            {text_array(g["image_urls"])}
        );
        """)

    ingredient_to_recipe = {}
    for recipe_id, ingredient_names in conn.execute(f"SELECT recipe_id, ingredient_names FROM recipes"):
        ingredient_ids = []
        for i in ingredient_names:
            result = conn.execute(f"SELECT ingredient_id FROM ingredients WHERE name={text(i)}").first()
            if result is None:
                ingredient_ids.append(None)
            else:
                ingredient_ids.append(result[0])
                if result[0] not in ingredient_to_recipe:
                    ingredient_to_recipe[result[0]] = set([])
                ingredient_to_recipe[result[0]].add(recipe_id)
        conn.execute(f"UPDATE recipes SET ingredient_ids={int_array(ingredient_ids)} WHERE recipe_id={json.dumps(recipe_id)}")

    for ingredient_id, recipe_ids in ingredient_to_recipe.items():
        conn.execute(f"UPDATE ingredients SET recipe_ids={int_array(list(recipe_ids))} WHERE ingredient_id={json.dumps(ingredient_id)}")

    i_to_g = {}
    r_to_g = {}
    for g_id, g_name in conn.execute(f"SELECT grocery_store_id, name FROM grocery_stores"):
        i_ids = [x["ingredient_id"] for x in grocery_links if x["grocery_store_id"] == g_id]
        r_ids = list(set([r_id for i_id in i_ids for r_id in ingredient_to_recipe[i_id]]))
        for i_id in i_ids:
            if i_id not in i_to_g:
                i_to_g[i_id] = set([])
            i_to_g[i_id].add(g_id)
        for r_id in r_ids:
            if r_id not in r_to_g:
                r_to_g[r_id] = set([])
            r_to_g[r_id].add(r_id)
        conn.execute(f"UPDATE grocery_stores SET ingredient_ids={int_array(list(i_ids))}, recipe_ids={int_array(list(r_ids))} WHERE grocery_store_id={json.dumps(g_id)}")

    for i_id, in conn.execute(f"SELECT ingredient_id FROM ingredients"):
        conn.execute(f"UPDATE ingredients SET grocery_store_ids={int_array(list(i_to_g[i_id]))} WHERE ingredient_id={i_id}")
    for r_id, in conn.execute(f"SELECT recipe_id FROM recipes"):
        conn.execute(f"UPDATE recipes SET grocery_store_ids={int_array(list(r_to_g[r_id]))} WHERE recipe_id={r_id}")
