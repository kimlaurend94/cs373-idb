from flask import jsonify, request
from flask_cors import cross_origin

from query_helpers import object_as_dict, get_query
from models import Recipe, Ingredient, Store
from filter_recipes import *
from filter_ingredients import *
from filter_stores import *

from app import db


def register_routes(app):
    @app.route("/")
    def index():
        return "<h2>Welcome to Mealmaker API!</h2>"

    @app.route("/yolo")
    def yolo():
        return "<h2>yolo</h2>"

    @app.route("/recipes", methods=["GET"])
    @cross_origin()
    def recipe():
        queries = request.args.to_dict(flat=False)
        recipe_query = db.session.query(Recipe)

        page = queries.get("page", None)
                     
        if page is None:
            page = -1
        else:
            page = int(page[0])
        li = []

        # searching
        search = get_query("search", queries)
        if search:
            recipe_query = search_recipes(search, recipe_query)

        # filtering
        recipe_query = filter_recipes(recipe_query, queries)

        # sorting
        sort = get_query("sort", queries)
        recipe_query = sort_recipes(sort, recipe_query)

        count = recipe_query.count()

        if page != -1:
            per_page = (
                int(get_query("perPage", queries).pop())
                if get_query("perPage", queries)
                else 100
            )
            recipes = recipe_query.paginate(page=page, per_page=per_page)

            for r in recipes.items:
                curr_dict = {
                    "recipe_id": r.recipe_id,
                    "name": r.name,
                    "description": r.description,
                    "credits": r.credits,
                    "prep_time": r.prep_time,
                    "cook_time": r.cook_time,
                    "total_time": r.total_time,
                    "num_servings": r.num_servings,
                    "ingredients": r.ingredients,
                    "ingredient_weights": r.ingredient_weights,
                    "ingredient_quantities": r.ingredient_quantities,
                    "ingredient_measures": r.ingredient_measures,
                    "ingredient_names": r.ingredient_names,
                    "health_labels": r.health_labels,
                    "instructions": r.instructions,
                    "equipment": r.equipment,
                    "occasion": r.occasion,
                    "cuisine": r.cuisine,
                    "video_url": r.video_url,
                    "thumbnail_url": r.thumbnail_url,
                    "ingredient_ids": r.ingredient_ids,
                    "grocery_store_ids": r.grocery_store_ids,
                }
                li.append(curr_dict)
        else:
            for r in recipe_query:
                curr_dict = {
                    "recipe_id": r.recipe_id,
                    "name": r.name,
                    "description": r.description,
                    "credits": r.credits,
                    "prep_time": r.prep_time,
                    "cook_time": r.cook_time,
                    "total_time": r.total_time,
                    "num_servings": r.num_servings,
                    "ingredients": r.ingredients,
                    "ingredient_weights": r.ingredient_weights,
                    "ingredient_quantities": r.ingredient_quantities,
                    "ingredient_measures": r.ingredient_measures,
                    "ingredient_names": r.ingredient_names,
                    "health_labels": r.health_labels,
                    "instructions": r.instructions,
                    "equipment": r.equipment,
                    "occasion": r.occasion,
                    "cuisine": r.cuisine,
                    "video_url": r.video_url,
                    "thumbnail_url": r.thumbnail_url,
                    "ingredient_ids": r.ingredient_ids,
                    "grocery_store_ids": r.grocery_store_ids,
                }
                li.append(curr_dict)

        return jsonify({"page": li, "count": count})

    @app.route("/recipes/<id>", methods=["GET"])
    @cross_origin()
    def get_recipe_detail(id):
        recipe = Recipe.query.get(id)
        d = object_as_dict(recipe)
        return jsonify(d)

    @app.route("/ingredients", methods=["GET"])
    @cross_origin()
    def ingredients():
        queries = request.args.to_dict(flat=False)
        ingredient_query = db.session.query(Ingredient)

        page = queries.get("page", None)
        if page is None:
            page = -1
        else:
            page = int(page[0])
        li = []

        # searching
        search = get_query("search", queries)
        if search:
            ingredient_query = search_ingredients(search, ingredient_query)

        # filtering
        ingredient_query = filter_ingredients(ingredient_query, queries)

        # sorting
        sort = get_query("sort", queries)
        ingredient_query = sort_ingredients(sort, ingredient_query)

        count = ingredient_query.count()

        if page != -1:
            per_page = (
                int(get_query("perPage", queries).pop())
                if get_query("perPage", queries)
                else 200
            )

            ingredients = ingredient_query.paginate(page=page, per_page=per_page)
            for r in ingredients.items:
                curr_dict = {
                    "ingredient_id": r.ingredient_id,
                    "name": r.name,
                    "diet_labels": r.diet_labels,
                    "health_labels": r.health_labels,
                    "cautions": r.cautions,
                    "calories": r.calories,
                    "protein": r.protein,
                    "fat": r.fat,
                    "saturated_fat": r.saturated_fat,
                    "trans_fat": r.trans_fat,
                    "carbs": r.carbs,
                    "fiber": r.fiber,
                    "sugar": r.sugar,
                    "added_sugar": r.added_sugar,
                    "cholesterol": r.cholesterol,
                    "image_urls": r.image_urls,
                    "recipe_ids": r.recipe_ids,
                    "grocery_store_ids": r.grocery_store_ids,
                }
                li.append(curr_dict)
        else:
            for r in ingredient_query:
                curr_dict = {
                    "ingredient_id": r.ingredient_id,
                    "name": r.name,
                    "diet_labels": r.diet_labels,
                    "health_labels": r.health_labels,
                    "cautions": r.cautions,
                    "calories": r.calories,
                    "protein": r.protein,
                    "fat": r.fat,
                    "saturated_fat": r.saturated_fat,
                    "trans_fat": r.trans_fat,
                    "carbs": r.carbs,
                    "fiber": r.fiber,
                    "sugar": r.sugar,
                    "added_sugar": r.added_sugar,
                    "cholesterol": r.cholesterol,
                    "image_urls": r.image_urls,
                    "recipe_ids": r.recipe_ids,
                    "grocery_store_ids": r.grocery_store_ids,
                }
                li.append(curr_dict)

        return jsonify({"page": li, "count": count})

    @app.route("/ingredients/<id>", methods=["GET"])
    @cross_origin()
    def get_ingredient_detail(id):
        ingredient = Ingredient.query.get(id)
        d = object_as_dict(ingredient)
        return jsonify(d)

    @app.route("/stores", methods=["GET"])
    @cross_origin()
    def grocery_stores():
        queries = request.args.to_dict(flat=False)
        store_query = db.session.query(Store)

        page = queries.get("page", None)
        if page is None:
            page = -1
        else:
            page = int(page[0])
        li = []

        # searching
        search = get_query("search", queries)
        if search:
            store_query = search_stores(search, store_query)

        # filtering
        store_query = filter_stores(store_query, queries)

        # sorting
        sort = get_query("sort", queries)
        store_query = sort_stores(sort, store_query)

        count = store_query.count()

        if page != -1:
            per_page = (
                int(get_query("perPage", queries).pop())
                if get_query("perPage", queries)
                else 200
            )
        
            stores = store_query.paginate(page=page, per_page=per_page)
            for r in stores.items:
                curr_dict = {
                    "grocery_store_id": r.grocery_store_id,
                    "name": r.name,
                    "city": r.city,
                    "latitude": r.latitude,
                    "longitude": r.longitude,
                    "address": r.address,
                    "phone": r.phone,
                    "curbside_pickup": r.has_curbside_pickup,
                    "delivery": r.has_delivery,
                    "opening_hours": r.opening_hours,
                    "price": r.price,
                    "rating": r.rating,
                    "num_ratings": r.num_ratings,
                    "website": r.website,
                    "maps_url": r.maps_url,
                    "image_urls": r.image_urls,
                    "recipe_ids": r.recipe_ids,
                    "ingredient_ids": r.ingredient_ids,
                }
                li.append(curr_dict)
        else:
            for r in store_query:
                curr_dict = {
                    "grocery_store_id": r.grocery_store_id,
                    "name": r.name,
                    "city": r.city,
                    "latitude": r.latitude,
                    "longitude": r.longitude,
                    "address": r.address,
                    "phone": r.phone,
                    "curbside_pickup": r.has_curbside_pickup,
                    "delivery": r.has_delivery,
                    "opening_hours": r.opening_hours,
                    "price": r.price,
                    "rating": r.rating,
                    "num_ratings": r.num_ratings,
                    "website": r.website,
                    "maps_url": r.maps_url,
                    "image_urls": r.image_urls,
                    "recipe_ids": r.recipe_ids,
                    "ingredient_ids": r.ingredient_ids,
                }
                li.append(curr_dict)
        return jsonify({"page": li, "count": count})

    @app.route("/stores/<id>", methods=["GET"])
    @cross_origin()
    def get_store_detail(id):
        store = Store.query.get(id)
        d = object_as_dict(store)
        return jsonify(d)
