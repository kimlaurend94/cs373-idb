import os

from flask import Flask
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

db = SQLAlchemy()
ma = Marshmallow()
cors = CORS()

# Referenced StayFresh repo
# https://gitlab.com/cs373-11am-group12/stayfresh/-/blob/29e046aefb0731ca409c9ad81d52d141381f9109/backend/app.py


def create_app(testing=False):
    app = Flask(__name__)
    app.debug = True
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    user = os.environ["POSTGRES_USER"]
    password = os.environ["POSTGRES_PASSWORD"]
    host = os.environ["POSTGRES_HOST"]
    db_name = os.environ["POSTGRES_DB"]
    app.config["SQLALCHEMY_DATABASE_URI"] = f"postgresql+psycopg2://{user}:{password}@{host}:5432/{db_name}"

    if testing:
        app.config["TESTING"] = True

    db.init_app(app)
    ma.init_app(app)
    cors.init_app(app)

    from routes import register_routes

    register_routes(app)
    return app


if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0", port=3000, debug=True)
