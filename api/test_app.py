import pytest
from app import create_app, db
import models
import json


@pytest.fixture()
def app():
    app = create_app(testing=True)
    with app.app_context():
        db.create_all()
        db.session.add(models.Recipe(name="Recipe 1", prep_time=7))
        db.session.add(models.Recipe(name="Recipe 2", prep_time=3))
        db.session.add(models.Recipe(name="Recipe 3", prep_time=11))
        db.session.add(models.Ingredient(name="Ingredient 1", cholesterol=2.0))
        db.session.add(models.Ingredient(name="Ingredient 2", cholesterol=1.0))
        db.session.add(models.Store(name="Store 1 apple"))
        db.session.add(models.Store(name="Store 2 banana"))
        db.session.add(models.Store(name="Store 3 orange"))
        db.session.add(models.Store(name="Store 4 apple"))
        db.session.commit()

    yield app

    with app.app_context():
        db.drop_all()


@pytest.fixture()
def client(app):
    return app.test_client()


def test_index(client):
    assert client.get("/").data == b"<h2>Welcome to Mealmaker API!</h2>"


def test_recipes(client):
    data = json.loads(client.get("/recipes").data)
    assert data["count"] == 3
    assert len(data["page"]) == 3


def test_recipes_filter(client):
    data = json.loads(client.get("/recipes?prep_time=5-10").data)
    assert data["count"] == 1
    assert len(data["page"]) == 1


def test_recipe(client):
    data = json.loads(client.get("/recipes/1").data)
    assert data["name"] == "Recipe 1"


def test_ingredients(client):
    data = json.loads(client.get("/ingredients").data)
    assert data["count"] == 2
    assert len(data["page"]) == 2


def test_ingredients_sort(client):
    data1 = json.loads(client.get("/ingredients?sort=cholesterol").data)
    data2 = json.loads(client.get("/ingredients?sort=desc-cholesterol").data)
    assert data1["page"][0]["name"] == data2["page"][1]["name"] == "Ingredient 2"
    assert data1["page"][1]["name"] == data2["page"][0]["name"] == "Ingredient 1"


def test_ingredient(client):
    data = json.loads(client.get("/ingredients/2").data)
    assert data["name"] == "Ingredient 2"


def test_stores(client):
    data = json.loads(client.get("/stores").data)
    assert data["count"] == 4
    assert len(data["page"]) == 4


def test_stores_search(client):
    data = json.loads(client.get("/stores?search=apple").data)
    assert data["count"] == 2
    assert len(data["page"]) == 2


def test_store(client):
    data = json.loads(client.get("/stores/3").data)
    assert data["name"] == "Store 3 orange"
