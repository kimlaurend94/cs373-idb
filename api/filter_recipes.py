from sqlalchemy.sql.expression import null
from models import Recipe, db

from sqlalchemy import and_, or_, func, case
from query_helpers import *
from collections import Counter

def filter_recipes_by(recipe_query, filtering, what):
    if filtering == "prep_time":
        recipe_query = recipe_query.filter(and_
            ((Recipe.prep_time) >= what[0], (Recipe.prep_time) <= what[1],)
        )
    elif filtering == "cook_time":
        recipe_query = recipe_query.filter(and_
            ((Recipe.cook_time) >= what[0], (Recipe.cook_time) <= what[1],)
        )
    elif filtering == "total_time":
        recipe_query = recipe_query.filter(and_
            ((Recipe.total_time) >= what[0], (Recipe.total_time) <= what[1],)
        )
    elif filtering == "num_servings":
        recipe_query = recipe_query.filter(and_
            ((Recipe.num_servings) >= what[0], (Recipe.num_servings) <= what[1],)
        )
    elif filtering == "health_labels":
        recipe_query = recipe_query.filter(Recipe.health_labels.contains([what[0]]))
    elif filtering == "equipment":
        recipe_query = recipe_query.filter(Recipe.equipment.contains([what[0]]))

    return recipe_query


def filter_recipes(recipe_query, queries):
    prep_time = get_query("prep_time", queries)
    cook_time = get_query("cook_time", queries)
    total_time = get_query("total_time", queries)
    num_servings = get_query("num_servings", queries)
    health_labels = get_query("health_labels", queries)
    equipment = get_query("equipment", queries)
    name = get_query("name", queries)

    if prep_time is not None:
        prep_time = prep_time[0]
        minTime = 0
        maxTime = 99999
        if len(prep_time.split("-")) < 2:
            maxTime = prep_time
            # e.g. http://api.mealmaker.me/recipes?prep_time=5 recipes with prep_time <= 5
        else:
            minTime, maxTime = prep_time.split("-")
            # e.g. http://api.mealmaker.me/recipes?prep_time=5-10 recipes with prep_time 5-10

        recipe_query = filter_recipes_by(recipe_query, "prep_time", [minTime, maxTime])
    if cook_time is not None:
        cook_time = cook_time[0]
        minTime = 0
        maxTime = 99999
        if len(cook_time.split("-")) < 2:
            maxTime = cook_time
        else:
            minTime, maxTime = cook_time.split("-")
        recipe_query = filter_recipes_by(recipe_query, "cook_time", [minTime, maxTime])
    if total_time is not None:
        total_time = total_time[0]
        minTime = 0
        maxTime = 99999
        if len(total_time.split("-")) < 2:
            maxTime = total_time
        else:
            minTime, maxTime = total_time.split("-")
        recipe_query = filter_recipes_by(recipe_query, "total_time", [minTime, maxTime])
    if num_servings is not None:
        num_servings = num_servings[0]
        minServings = 0
        maxServings = 99999
        if len(num_servings.split("-")) < 2:
            minServings = num_servings
            # http://api.mealmaker.me/recipes?num_servings=10 recipes with >= 10 num_servings
        else:
            minServings, maxServings = num_servings.split("-")
        recipe_query = filter_recipes_by(recipe_query, "num_servings", [minServings, maxServings])
    if health_labels is not None:
        recipe_query = filter_recipes_by(recipe_query, "health_labels", health_labels)
    if equipment is not None:
        recipe_query = filter_recipes_by(recipe_query, "equipment", equipment)
    if name != None:
        searches = []
        for term in name:
            searches.append(func.lower(Recipe.name).contains(term.lower()))

        return recipe_query.filter(and_(*tuple(searches)))

    return recipe_query


def sort_recipe_by(sorting, recipe_query, desc):
    recipe = None

    if sorting == "prep_time":
        recipe = Recipe.prep_time
    elif sorting == "cook_time":
        recipe = Recipe.cook_time
    elif sorting == "total_time":
        recipe = Recipe.total_time
    elif sorting == "num_servings":
        recipe = Recipe.num_servings
    elif sorting == "equipment":
        recipe = Recipe.equipment
    elif sorting == "name":
        recipe = func.lower(Recipe.name)
    else:
        return recipe_query

    if desc:
        return recipe_query.order_by(recipe.desc())
    else:
        return recipe_query.order_by(recipe)


def sort_recipes(sort, recipe_query):
    if sort == None:
        return recipe_query
    else:
        sort = sort[0]

    # e.g. ?sort=desc-prep_time
    sort = sort.split("-")

    if len(sort) > 1:
        return sort_recipe_by(sort[1], recipe_query, True)
    else:
        return sort_recipe_by(sort[0], recipe_query, False)


# return all relations with attributes that contain or equal param term 
def search_query(recipe_query, term):
    # list to hold params for filter()
    searches = []
    searches.append(func.lower(Recipe.name).contains(term))
    searches.append(func.lower(Recipe.credits).contains(term))
    searches.append(func.lower(Recipe.description).contains(term))
    if (term.isdigit()):
        searches.append(Recipe.prep_time == term)
        searches.append(Recipe.cook_time == term)
        searches.append(Recipe.total_time == term)
        searches.append(Recipe.num_servings == term)

    # dealing with arrays, requires array values to start with "{"        
    try:
        float(term)
        term = "{"+term+"}"
        searches.append(Recipe.ingredient_weights.contains(term))
        searches.append(Recipe.ingredient_quantities.contains(term))
        searches.append(Recipe.ingredient_measures.contains(term))
    except ValueError:
        pass
    term = "{"+term+"}"
    searches.append(Recipe.ingredients.contains(term))
    searches.append(Recipe.instructions.contains(term))
    searches.append(Recipe.health_labels.contains(term))
    searches.append(Recipe.equipment.contains(term))

    searches.append((Recipe.cuisine).contains(term))
    searches.append((Recipe.occasion).contains(term))
    searches.append((Recipe.ingredient_names).contains(term))

    # filter by params stored in searches array
    recipe_query = recipe_query.filter(or_(*tuple(searches)))

    return recipe_query


def search_recipes(search, recipe_query):
    if not search:
        return recipe_query
    search = search[0].strip()

    # split by whitespace
    terms = search.split()
    if (len(terms) < 1):
        return recipe_query

    # search for first term and append all corresponding recipe_ids
    i = 0
    term = terms[0]
    common = ['the', 'and', 'of', 'an', 'a']
    if (term in common and len(terms) == 1):
        return recipe_query
    if (term in common):
        i += 1
    matched_ids = []
    matched_relations = search_query(recipe_query, terms[i])
    for relation in matched_relations:
        matched_ids.append(relation.recipe_id)

    # search for remaining terms
    for num in range(i, len(terms)):        
        term = terms[num]
        term = term.lower()
        # filter out common words
        if term in common:
            continue

        term_matches = search_query(recipe_query, term)
        for relation in term_matches:
            matched_ids.append(relation.recipe_id)
        matched_relations = matched_relations.union(term_matches)

    # contains ids in descending frequency order (contains duplicates)
    # relevance determined by number of terms matched
    sorted_ids_dup = [n for n,count in Counter(matched_ids).most_common() for i in range(count)]
    id_ordering = case({_id: index for index, _id in enumerate(sorted_ids_dup)}, value=Recipe.recipe_id)
    return matched_relations.order_by(id_ordering)
