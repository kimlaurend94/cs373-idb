from sqlalchemy.sql.expression import null
from models import Ingredient, db

from sqlalchemy import and_, or_, func, case
from query_helpers import *
from collections import Counter


def filter_ingredients_by(ingredient_query, filtering, what):
    if filtering == "cholesterol":
        ingredient_query = ingredient_query.filter(and_
            ((Ingredient.cholesterol) >= what[0], (Ingredient.cholesterol) <= what[1],)
        )
    elif filtering == "calories":
        ingredient_query = ingredient_query.filter(and_
            ((Ingredient.calories) >= what[0], (Ingredient.calories) <= what[1],)
        )
    elif filtering == "carbs":
        ingredient_query = ingredient_query.filter(and_
            ((Ingredient.carbs) >= what[0], (Ingredient.carbs) <= what[1],)
        )
    elif filtering == "protein":
        ingredient_query = ingredient_query.filter(and_
            ((Ingredient.protein) >= what[0], (Ingredient.protein) <= what[1],)
        )
    elif filtering == "sugar":
        ingredient_query = ingredient_query.filter(and_
            ((Ingredient.sugar) >= what[0], (Ingredient.sugar) <= what[1],)
        )
    elif filtering == "fiber":
        ingredient_query = ingredient_query.filter(and_
            ((Ingredient.fiber) >= what[0], (Ingredient.fiber) <= what[1],)
        )
    elif filtering == "fat":
        ingredient_query = ingredient_query.filter(and_
            ((Ingredient.fat) >= what[0], (Ingredient.fat) <= what[1],)
        )
    
    return ingredient_query

def filter_ingredients(ingredient_query, queries):
    cholesterol = get_query("cholesterol", queries)
    calories = get_query("calories", queries)
    carbs = get_query("carbs", queries)
    protein = get_query("protein", queries)
    sugar = get_query("sugar", queries)
    fiber = get_query("fiber", queries)
    fat = get_query("fat", queries)
    name = get_query("name", queries)

    if cholesterol != None:
        cholesterol = cholesterol[0]
        min = 0.0
        max = 99999.0
        if len(cholesterol.split("-")) < 2:
            max = cholesterol
            # e.g. http://api.mealmaker.me/ingredients?cholesterol=5 ingredients with cholesterol <= 5
        else:
            min, max = cholesterol.split("-")
            # e.g. http://api.mealmaker.me/ingredients?cholesterol=5-10 ingredients with cholesterol 5-10
        ingredient_query = filter_ingredients_by(ingredient_query, "cholesterol", [min, max])
    if calories != None:
        calories = calories[0]
        min = 0.0
        max = 99999.0
        if len(calories.split("-")) < 2:
            max = calories
        else:
            min, max = calories.split("-")
        ingredient_query = filter_ingredients_by(ingredient_query, "calories", [min, max])
    if carbs != None:
        carbs = carbs[0] 
        min = 0.0
        max = 99999.0
        if len(carbs.split("-")) < 2:
            max = carbs
        else:
            min, max = carbs.split("-")
        ingredient_query = filter_ingredients_by(ingredient_query, "carbs", [min, max])
    if protein != None:
        protein = protein[0] 
        min = 0.0
        max = 99999.0
        if len(protein.split("-")) < 2:
            max = protein
        else:
            min, max = protein.split("-")
        ingredient_query = filter_ingredients_by(ingredient_query, "protein", [min, max])
    if sugar != None:
        sugar = sugar[0]
        min = 0
        max = 99999
        if len(sugar.split("-")) < 2:
            max = sugar
            # http://api.mealmaker.me/ingredients?sugar=1 ingredients with <= 1 sugar
        else:
            min, max = sugar.split("-")
        ingredient_query = filter_ingredients_by(ingredient_query, "sugar", [min, max])
    if fiber != None:
        fiber = fiber[0]
        min = 0
        max = 99999
        if len(fiber.split("-")) < 2:
            max = fiber
            # http://api.mealmaker.me/ingredients?sugar=10 ingredients with >= 10 fiber
        else:
            min, max = fiber.split("-")
        ingredient_query = filter_ingredients_by(ingredient_query, "fiber", [min, max])
    if fat != None:
        fat = fat[0]
        min = 0
        max = 99999
        if len(fat.split("-")) < 2:
            max = fat
            # http://api.mealmaker.me/ingredients?sugar=10 ingredients with >= 10 fiber
        else:
            min, max = fat.split("-")
        ingredient_query = filter_ingredients_by(ingredient_query, "fat", [min, max])
    if name != None:
        searches = []
        for term in name:
            searches.append(func.lower(Ingredient.name).contains(term.lower()))

        return ingredient_query.filter(or_(*tuple(searches)))

    return ingredient_query

def sort_ingredient_by(sorting, ingredient_query, desc):
    ingredient = None

    # e.g. http://api.mealmaker.me/ingredients?sort=cholesterol
    if sorting == "cholesterol":
        ingredient = Ingredient.cholesterol
    elif sorting == "calories":
        ingredient = Ingredient.calories
    elif sorting == "carbs":
        ingredient = Ingredient.carbs
    elif sorting == "protein":
        ingredient = Ingredient.protein
    elif sorting == "sugar":
        ingredient = Ingredient.sugar
    elif sorting == "fiber":
        ingredient = Ingredient.fiber
    elif sorting == "fat":
        ingredient = Ingredient.fat
    elif sorting == "name":
        ingredient = func.lower(Ingredient.name)
    else:
        return ingredient_query

    if desc:
        return ingredient_query.order_by(ingredient.desc())
    else:
        return ingredient_query.order_by(ingredient)


def sort_ingredients(sort, ingredient_query):
    if sort == None:
        return ingredient_query
    else:
        sort = sort[0]

    # ..?sort=desc-protein
    sort = sort.split("-")

    if len(sort) > 1:
        return sort_ingredient_by(sort[1], ingredient_query, True)
    else:
        return sort_ingredient_by(sort[0], ingredient_query, False)

# return all relations with attributes that contain or equal param term 
def search_query(ingredient_query, term):
    # list to hold params for filter()
    searches = []
    searches.append(func.lower(Ingredient.name).contains(term))
    try:
        float(term)
        searches.append(Ingredient.calories == term)
        searches.append(Ingredient.protein == term)
        searches.append(Ingredient.fat == term)
        searches.append(Ingredient.saturated_fat == term)
        searches.append(Ingredient.trans_fat == term)
        searches.append(Ingredient.carbs == term)
        searches.append(Ingredient.fiber == term)
        searches.append(Ingredient.sugar == term)
        searches.append(Ingredient.added_sugar == term)
        searches.append(Ingredient.cholesterol == term)
    except ValueError:
        pass
    # dealing with arrays, requires array values to start with "{"
    term = "{"+term+"}"
    term = term.upper()
    searches.append((Ingredient.cautions).contains(term))
    searches.append((Ingredient.diet_labels).contains(term))
    searches.append((Ingredient.health_labels).contains(term))

    # join all queries by or_
    ingredient_query = ingredient_query.filter(or_(*tuple(searches)))

    return ingredient_query


def search_ingredients(search, ingredient_query):
    if not search:
        return ingredient_query
    search = search[0].strip()

    # split by whitespace
    terms = search.split()
    if (len(terms) < 1):
        return ingredient_query

    # search for first term and append all corresponding ingredient_ids
    i = 0
    term = terms[0]
    common = ['the', 'and', 'of', 'an', 'a']
    if (term in common and len(terms) == 1):
        return ingredient_query
    if (term in common):
        i += 1
    matched_ids = []
    matched_relations = search_query(ingredient_query, terms[i])
    for relation in matched_relations:
        matched_ids.append(relation.ingredient_id)

    # search for remaining terms
    for num in range(i, len(terms)):        
        term = terms[num]
        term = term.lower()
        # filter out common words
        if term in common:
            continue

        term_matches = search_query(ingredient_query, term)
        for relation in term_matches:
            matched_ids.append(relation.ingredient_id)
        matched_relations = matched_relations.union(term_matches)

    # contains ids in descending frequency order (contains duplicates)
    # relevance determined by number of terms matched
    sorted_ids_dup = [n for n,count in Counter(matched_ids).most_common() for i in range(count)]
    id_ordering = case({_id: index for index, _id in enumerate(sorted_ids_dup)}, value=Ingredient.ingredient_id)
    return matched_relations.order_by(id_ordering)
