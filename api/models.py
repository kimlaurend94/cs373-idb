from app import db, ma
from sqlalchemy.dialects.postgresql import ARRAY


# Referenced StayFresh
# https://gitlab.com/cs373-11am-group12/stayfresh/-/blob/3cee161a11ff9cf0176dafc0df92072e6297f7cf/backend/models.py

# Connections between models #


# SCHEMAS #
class Recipe(db.Model):
    __tablename__ = "recipes"
    recipe_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.Text())
    credits = db.Column(db.String())
    prep_time = db.Column(db.Integer())
    cook_time = db.Column(db.Integer())
    total_time = db.Column(db.Integer())
    num_servings = db.Column(db.Integer())
    ingredients = db.Column(ARRAY(db.Text))
    ingredient_weights = db.Column(ARRAY(db.Float))
    ingredient_quantities = db.Column(ARRAY(db.Float))
    ingredient_measures = db.Column(ARRAY(db.Text))
    # ingredient_names = db.Column(db.ARRAY(db.Text))
    ingredient_names = db.Column(ARRAY(db.Text))
    health_labels = db.Column(ARRAY(db.Text))
    instructions = db.Column(ARRAY(db.Text))
    # equipment = db.Column(db.ARRAY(db.Text))
    equipment = db.Column(ARRAY(db.Text))
    # occasion = db.Column(db.ARRAY(db.Text))
    occasion = db.Column(ARRAY(db.Text))
    # cuisine = db.Column(db.ARRAY(db.Text))
    cuisine = db.Column(ARRAY(db.Text))
    video_url = db.Column(db.Text())
    thumbnail_url = db.Column(db.Text())
    ingredient_ids = db.Column(ARRAY(db.Integer))
    grocery_store_ids = db.Column(ARRAY(db.Integer))

    def __init__(
        self,
        name="",
        description=None,
        credits=None,
        prep_time=None,
        cook_time=None,
        total_time=None,
        num_servings=None,
        ingredients=None,
        ingredient_weights=None,
        ingredient_quantities=None,
        ingredient_measures=None,
        ingredient_names=None,
        health_labels=None,
        instructions=None,
        equipment=None,
        occasion=None,
        cuisine=None,
        video_url=None,
        thumbnail_url=None,
        ingredient_ids=None,
        grocery_store_ids=None,
    ):
        if instructions is None:
            instructions = []
        if ingredient_names is None:
            ingredient_names = []
        if health_labels is None:
            health_labels = []
        if ingredient_measures is None:
            ingredient_measures = []
        if ingredient_quantities is None:
            ingredient_quantities = []
        if ingredient_weights is None:
            ingredient_weights = []
        if ingredients is None:
            ingredients = []
        self.name = (name,)
        self.description = (description,)
        self.credits = (credits,)
        self.prep_time = (prep_time,)
        self.cook_time = (cook_time,)
        self.total_time = (total_time,)
        self.num_servings = (num_servings,)
        self.ingredients = (ingredients,)
        self.ingredient_weights = (ingredient_weights,)
        self.ingredient_quantities = (ingredient_quantities,)
        self.ingredient_measures = (ingredient_measures,)
        self.ingredient_names = (ingredient_names,)
        self.health_labels = (health_labels,)
        self.instructions = (instructions,)
        self.equipment = (equipment,)
        self.occasion = (occasion,)
        self.cuisine = (cuisine,)
        self.video_url = (video_url,)
        self.thumbnail_url = (thumbnail_url,)
        self.ingredient_ids = (ingredient_ids,)
        self.grocery_store_ids = grocery_store_ids


class Ingredient(db.Model):
    __tablename__ = "ingredients"
    ingredient_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    diet_labels = db.Column(ARRAY(db.Text))
    health_labels = db.Column(ARRAY(db.Text))
    cautions = db.Column(ARRAY(db.Text))
    calories = db.Column(db.Float())
    protein = db.Column(db.Float())
    fat = db.Column(db.Float())
    saturated_fat = db.Column(db.Float())
    trans_fat = db.Column(db.Float())
    carbs = db.Column(db.Float())
    fiber = db.Column(db.Float())
    sugar = db.Column(db.Float())
    added_sugar = db.Column(db.Float())
    cholesterol = db.Column(db.Float())
    image_urls = db.Column(ARRAY(db.Text))
    recipe_ids = db.Column(ARRAY(db.Integer))
    grocery_store_ids = db.Column(ARRAY(db.Integer))

    def __init__(
        self,
        name="",
        diet_labels=None,
        health_labels=None,
        cautions=None,
        calories=None,
        protein=None,
        fat=None,
        saturated_fat=None,
        trans_fat=None,
        carbs=None,
        fiber=None,
        sugar=None,
        added_sugar=None,
        cholesterol=None,
        image_urls=None,
        recipe_ids=None,
        grocery_store_ids=None,
    ):
        self.name = (name,)
        self.diet_labels = (diet_labels,)
        self.health_labels = (health_labels,)
        self.cautions = (cautions,)
        self.calories = (calories,)
        self.protein = (protein,)
        self.fat = (fat,)
        self.saturated_fat = (saturated_fat,)
        self.trans_fat = (trans_fat,)
        self.carbs = (carbs,)
        self.fiber = (fiber,)
        self.sugar = (sugar,)
        self.added_sugar = (added_sugar,)
        self.cholesterol = (cholesterol,)
        self.image_urls = (image_urls,)
        self.recipe_ids = (recipe_ids,)
        self.grocery_store_ids = grocery_store_ids


class Store(db.Model):
    __tablename__ = "grocery_stores"
    grocery_store_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    city = db.Column(db.String())
    latitude = db.Column(db.Float())
    longitude = db.Column(db.Float())
    address = db.Column(db.Text())
    phone = db.Column(db.String())
    has_curbside_pickup = db.Column(db.String())
    has_delivery = db.Column(db.String())
    opening_hours = db.Column(ARRAY(db.Text))
    price = db.Column(db.Integer())
    rating = db.Column(db.Float())
    num_ratings = db.Column(db.Integer())
    website = db.Column(db.Text())
    maps_url = db.Column(db.Text())
    image_urls = db.Column(ARRAY(db.Text))
    recipe_ids = db.Column(ARRAY(db.Integer))
    ingredient_ids = db.Column(ARRAY(db.Integer))

    def __init__(
        self,
        name="",
        city=None,
        latitude=None,
        longitude=None,
        address=None,
        phone=None,
        has_curbside_pickup=None,
        has_delivery=None,
        opening_hours=None,
        price=None,
        rating=None,
        num_ratings=None,
        website=None,
        maps_url=None,
        image_urls=None,
        recipe_ids=None,
        ingredient_ids=None,
    ):
        self.name = (name,)
        self.city = (city,)
        self.latitude = (latitude,)
        self.longitude = (longitude,)
        self.address = (address,)
        self.phone = (phone,)
        self.has_curbside_pickup = (has_curbside_pickup,)
        self.has_delivery = (has_delivery,)
        self.opening_hours = (opening_hours,)
        self.price = (price,)
        self.rating = (rating,)
        self.num_ratings = (num_ratings,)
        self.website = (website,)
        self.maps_url = (maps_url,)
        self.image_urls = (image_urls,)
        self.recipe_ids = (recipe_ids,)
        self.ingredient_ids = ingredient_ids


class RecipeSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "recipe_id",
            "name",
            "description",
            "credits",
            "prep_time",
            "cook_time",
            "total_time",
            "num_servings",
            "ingredients",
            "ingredient_weights",
            "ingredient_quantities",
            "ingredient_measures",
            "ingredient_names",
            "health_labels",
            "instructions",
            "equipment",
            "occasion",
            "cuisine",
            "video_URL",
            "thumbnail_URL",
            "ingredient_ids",
            "grocery_store_ids",
        )


class IngredientSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "ingredient_id",
            "name",
            "diet_labels",
            "health_labels",
            "cautions",
            "calories",
            "protein",
            "fat",
            "saturated_fat",
            "trans_fat",
            "carbs",
            "fiber",
            "sugar",
            "added_sugar",
            "cholesterol",
            "image_urls",
            "recipe_ids",
            "grocery_store_ids",
        )


class StoreSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "grocery_store_id",
            "name",
            "city",
            "latitude",
            "longitude",
            "address",
            "phone",
            "curbside_pickup",
            "delivery",
            "opening_hours",
            "price",
            "rating",
            "num_ratings",
            "website",
            "maps_url",
            "image_urls",
            "recipe_ids",
            "ingredient_ids",
        )
