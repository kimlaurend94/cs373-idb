from sqlalchemy.sql.expression import null
from models import Store, db

from sqlalchemy import and_, or_, func, case
from query_helpers import *
from collections import Counter

def filter_stores_by(store_query, filtering, what):
    if filtering == "price":
        store_query = store_query.filter(and_
            ((Store.price) >= what[0], (Store.price) <= what[1],)
        )
    elif filtering == "rating":
        store_query = store_query.filter(and_
            ((Store.rating) >= what[0], (Store.rating) <= what[1],)
        )
    elif filtering == "num_ratings":
        store_query = store_query.filter(and_
            ((Store.num_ratings) >= what[0], (Store.num_ratings) <= what[1],)
        )
    elif filtering == "curbside_pickup":
        store_query = store_query.filter(and_
            ((Store.has_curbside_pickup) == what[0],)
        )
    elif filtering == "delivery":
        store_query = store_query.filter(and_
            ((Store.has_delivery) == what[0],)
        )
    elif filtering == "name":
        store_query = store_query.filter(
            func.lower(Store.name).contains(what[0].lower()))
    return store_query


def filter_stores(store_query, queries):
    price = get_query("price", queries)
    rating = get_query("rating", queries)
    num_ratings = get_query("num_ratings", queries)
    curbside_pickup = get_query("curbside_pickup", queries)
    delivery = get_query("delivery", queries)
    name = get_query("name", queries)

    if price != None:
        price = price[0]
        min = 0
        max = 99999
        if len(price.split("-")) < 2:
            max = price
            # e.g. http://api.mealmaker.me/stores?price=5 stores with price <= 5
        else:
            min, max = price.split("-")
            # e.g. http://api.mealmaker.me/stores?price=0-2 stores with price 0-2
        store_query = filter_stores_by(store_query, "price", [min, max])
    if rating != None:
        rating = rating[0]
        min = 0.0
        max = 99999.0
        if len(rating.split("-")) < 2:
            min = rating
        else:
            min, max = rating.split("-")
        store_query = filter_stores_by(store_query, "rating", [min, max])
    if num_ratings != None:
        num_ratings = num_ratings[0]
        min = 0
        max = 99999
        if len(num_ratings.split("-")) < 2:
            min = num_ratings
        else:
            min, max = num_ratings.split("-")
        store_query = filter_stores_by(store_query, "num_ratings", [min, max])
    if curbside_pickup != None:
        curbside_pickup = curbside_pickup[0]
        store_query = filter_stores_by(store_query, "curbside_pickup", curbside_pickup)
    if delivery != None:
        delivery = delivery[0]
        store_query = filter_stores_by(store_query, "delivery", delivery)
    if name != None:
        searches = []
        for term in name:
            searches.append(func.lower(Store.name).contains(term.lower()))

        return store_query.filter(or_(*tuple(searches)))

    return store_query

def sort_store_by(sorting, store_query, desc):
    store = None

    # e.g. http://api.mealmaker.me/stores?sort=price
    if sorting == "price":
        store = Store.price
    elif sorting == "rating":
        store = Store.rating
    elif sorting == "num_ratings":
        store = Store.num_ratings
    elif sorting == "curbside_pickup":
        store = Store.has_curbside_pickup
    elif sorting == "delivery":
        store = Store.has_delivery
    elif sorting == "name":
        store = func.lower(Store.name)
    else:
        return store_query

    if desc:
        return store_query.order_by(store.desc())
    else:
        return store_query.order_by(store)


def sort_stores(sort, store_query):
    if sort == None:
        return store_query
    else:
        sort = sort[0]

    # ..?sort=desc-delivery
    sort = sort.split("-")

    if len(sort) > 1:
        return sort_store_by(sort[1], store_query, True)
    else:
        return sort_store_by(sort[0], store_query, False)


# return all relations with attributes that contain or equal param term 
def search_query(store_query, term):
    searches = []
    try:
        float(term)
        if (term.isdigit()):
            searches.append(Store.price == term)
            searches.append(Store.num_ratings == term)
        else:
            searches.append(Store.latitude == term)
            searches.append(Store.longitude == term)
            searches.append(Store.rating == term)
    except ValueError:
        pass
    searches.append(func.lower(Store.name).contains(term))
    searches.append(func.lower(Store.city).contains(term))
    searches.append(func.lower(Store.address).contains(term))
    searches.append(func.lower(Store.phone).contains(term))
    searches.append(func.lower(Store.website).contains(term))
    searches.append(func.lower(Store.maps_url).contains(term))

    if term == 'curbside':
        searches.append(Store.has_curbside_pickup == "true")
    if term == 'delivery':
        searches.append(Store.has_delivery == "true")

    # join all queries by or_
    store_query = store_query.filter(or_(*tuple(searches)))

    return store_query


def search_stores(search, store_query):
    if not search:
        return store_query
    search = search[0].strip()

    # split by whitespace
    terms = search.split()
    if (len(terms) < 1):
        return store_query

    # search for first term and append all corresponding grocery_store_ids
    i = 0
    term = terms[0]
    common = ['the', 'and', 'of', 'an', 'a']
    if (term in common and len(terms) == 1):
        return store_query
    if (term in common):
        i += 1
    matched_ids = []
    matched_relations = search_query(store_query, terms[i])
    for relation in matched_relations:
        matched_ids.append(relation.grocery_store_id)

    # search for remaining terms
    for num in range(i, len(terms)):        
        term = terms[num]
        term = term.lower()
        # filter out common words
        if term in common:
            continue

        term_matches = search_query(store_query, term)
        for relation in term_matches:
            matched_ids.append(relation.grocery_store_id)
        matched_relations = matched_relations.union(term_matches)

    # contains ids in descending frequency order (contains duplicates)
    # relevance determined by number of terms matched
    sorted_ids_dup = [n for n,count in Counter(matched_ids).most_common() for i in range(count)]
    id_ordering = case({_id: index for index, _id in enumerate(sorted_ids_dup)}, value=Store.grocery_store_id)
    return matched_relations.order_by(id_ordering)
